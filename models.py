# coding=utf-8
import datetime
from operator import not_
from time import strftime, gmtime, localtime
from flask import json
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, mapper
from flask_login import UserMixin

db = SQLAlchemy()
base = declarative_base()


class MyModel(db.Model):
    __abstract__ = True

    # Stores changes made to this model's attributes. Can be retrieved
    # with model.changes
    _changes = {}

    def __init__(self, **kwargs):
        kwargs['_force'] = True
        self._set_columns(**kwargs)

    def _set_columns(self, **kwargs):
        force = kwargs.get('_force')

        readonly = []
        if hasattr(self, 'readonly_fields'):
            readonly = self.readonly_fields
        if hasattr(self, 'hidden_fields'):
            readonly += self.hidden_fields

        readonly += [
            'id',
            'created',
            'updated',
            'modified',
            'created_at',
            'updated_at',
            'modified_at',
        ]

        changes = {}

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()

        for key in columns:
            allowed = True if force or key not in readonly else False
            exists = True if key in kwargs else False
            if allowed and exists:
                val = getattr(self, key)
                if val != kwargs[key]:
                    changes[key] = {'old': val, 'new': kwargs[key]}
                    setattr(self, key, kwargs[key])

        for rel in relationships:
            allowed = True if force or rel not in readonly else False
            exists = True if rel in kwargs else False
            if allowed and exists:
                is_list = self.__mapper__.relationships[rel].uselist
                if is_list:
                    valid_ids = []
                    query = getattr(self, rel)
                    cls = self.__mapper__.relationships[rel].argument()
                    for item in kwargs[rel]:
                        if 'id' in item and query.filter_by(id=item['id']).limit(1).count() == 1:
                            obj = cls.query.filter_by(id=item['id']).first()
                            col_changes = obj.set_columns(**item)
                            if col_changes:
                                col_changes['id'] = str(item['id'])
                                if rel in changes:
                                    changes[rel].append(col_changes)
                                else:
                                    changes.update({rel: [col_changes]})
                            valid_ids.append(str(item['id']))
                        else:
                            col = cls()
                            col_changes = col.set_columns(**item)
                            query.append(col)
                            db.session.flush()
                            if col_changes:
                                col_changes['id'] = str(col.id)
                                if rel in changes:
                                    changes[rel].append(col_changes)
                                else:
                                    changes.update({rel: [col_changes]})
                            valid_ids.append(str(col.id))

                    # delete related rows that were not in kwargs[rel]
                    for item in query.filter(not_(cls.id.in_(valid_ids))).all():
                        col_changes = {
                            'id': str(item.id),
                            'deleted': True,
                        }
                        if rel in changes:
                            changes[rel].append(col_changes)
                        else:
                            changes.update({rel: [col_changes]})
                        db.session.delete(item)

                else:
                    val = getattr(self, rel)
                    if self.__mapper__.relationships[rel].query_class is not None:
                        if val is not None:
                            col_changes = val.set_columns(**kwargs[rel])
                            if col_changes:
                                changes.update({rel: col_changes})
                    else:
                        if val != kwargs[rel]:
                            setattr(self, rel, kwargs[rel])
                            changes[rel] = {'old': val, 'new': kwargs[rel]}

        return changes

    def set_columns(self, **kwargs):
        self._changes = self._set_columns(**kwargs)
        if 'modified' in self.__table__.columns:
            self.modified = datetime.utcnow()
        if 'updated' in self.__table__.columns:
            self.updated = datetime.utcnow()
        if 'modified_at' in self.__table__.columns:
            self.modified_at = datetime.utcnow()
        if 'updated_at' in self.__table__.columns:
            self.updated_at = datetime.utcnow()
        return self._changes

    @property
    def changes(self):
        return self._changes

    def reset_changes(self):
        self._changes = {}

    def to_dict(self, show=None, hide=None, path=None, show_all=None):
        """ Return a dictionary representation of this model.
        """

        if not show:
            show = []
        if not hide:
            hide = []
        hidden = []
        if hasattr(self, 'hidden_fields'):
            hidden = self.hidden_fields
        default = []
        if hasattr(self, 'default_fields'):
            default = self.default_fields

        ret_data = {}

        if not path:
            path = self.__tablename__.lower()

            def prepend_path(item):
                item = item.lower()
                if item.split('.', 1)[0] == path:
                    return item
                if len(item) == 0:
                    return item
                if item[0] != '.':
                    item = '.%s' % item
                item = '%s%s' % (path, item)
                return item

            show[:] = [prepend_path(x) for x in show]
            hide[:] = [prepend_path(x) for x in hide]

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()
        properties = dir(self)

        for key in columns:
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or key == 'id' or check in show or key in default:
                ret_data[key] = getattr(self, key)

        for key in relationships:
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or check in show or key in default:
                hide.append(check)
                is_list = self.__mapper__.relationships[key].uselist
                if is_list:
                    ret_data[key] = []
                    for item in getattr(self, key):
                        ret_data[key].append(item.to_dict(
                            show=show,
                            hide=hide,
                            path=('%s.%s' % (path, key.lower())),
                            show_all=show_all,
                        ))
                else:
                    if self.__mapper__.relationships[key].query_class is not None:
                        ret_data[key] = getattr(self, key).to_dict(
                            show=show,
                            hide=hide,
                            path=('%s.%s' % (path, key.lower())),
                            show_all=show_all,
                        )
                    else:
                        ret_data[key] = getattr(self, key)

        for key in list(set(properties) - set(columns) - set(relationships)):
            if key.startswith('_'):
                continue
            check = '%s.%s' % (path, key)
            if check in hide or key in hidden:
                continue
            if show_all or check in show or key in default:
                val = getattr(self, key)
                try:
                    ret_data[key] = json.loads(json.dumps(val))
                except:
                    pass

        return ret_data

class User(UserMixin, MyModel):
    __tablename__ = 'user'
    id_usr = db.Column('ID_USR', db.INTEGER, primary_key=True, autoincrement=True)
    id_rgn = db.Column(db.Integer, db.ForeignKey('region.ID_RGN'), nullable=False)
    username_usr = db.Column('USERNAME_USR', db.VARCHAR)
    pass_usr = db.Column('PASS_USR', db.VARCHAR)
    etat_usr = db.Column('ETAT_USR', db.VARCHAR)
    type_usr = db.Column('TYPE_USR', db.VARCHAR)
    date_create_usr = db.Column('DATE_CREATE_USR', db.DATETIME)

    def __init__(self, pseudo, password, etat, type):
        self.username_usr = pseudo
        self.pass_usr = password
        self.etat_usr = etat
        self.type_usr = type
        self.date_create_usr = strftime("%Y-%m-%d H%:M%:s%", gmtime())

    def __repr__(self):
        return "compte %s " % self.id_usr

    def is_active(self):
        if self.etat_usr == 'active':
            return True
        else:
            return False

    def get_id(self):
        return self.id_usr

    def commit(self):
        db.session.add(self)
        db.session.commit()
        pass

    def todict(self):
        return ({'id': self.id_usr, 'pseudo': self.username_usr, 'etat': self.etat_usr,
                 'type': self.type_usr})


class Budget(MyModel):
    __tablename__ = 'budget_prev'
    __bind_key__ = 'prevision'
    id_bdg = db.Column('ID_BDG', db.INTEGER, primary_key=True, autoincrement=True)
    nom_bdg = db.Column('NOM_BDG', db.VARCHAR)
    type_bdg = db.Column('TYPE_BDG', db.VARCHAR)
    solde_init_dbg = db.Column('SOLDE_INIT_BDG', db.Numeric(45,2))
    solde_actuel_bdg = db.Column('SOLDE_ACTUEL_BDG', db.Numeric(45,2))
    # annee_bdg = db.Column('ANNEE_BDG', db.INTEGER)
    
    def __init__(self, nom,typeb, solde_init, solde_act):
        self.nom_bdg = nom
        self.type_bdg = typeb
        self.solde_init_dbg = solde_init
        self.solde_actuel_bdg = solde_act

    def __repr__(self):
        return "Budget %s " % self.id_bdg
        
    def commit(self):
        db.session.add(self)
        db.session.commit()
        pass
    def tograph(self):
        childs=[]
        chaps= Chapitre.query.filter_by(id_bdg=self.id_bdg).all()
        for c in chaps:
            childs.append(c.tograph())
        return ({
            'name': self.nom_bdg,
            'value': 1 if (Chapitre.query.filter_by(id_bdg=self.id_bdg).count()==0) else Chapitre.query.filter_by(id_bdg=self.id_bdg).count() ,
            'children': childs
            })
    def todict(self):
        return ({'id': self.id_bdg, 'nom': self.nom_bdg, 'solde_init': self.solde_init_dbg,
                 'solde_actuel': self.solde_actuel_bdg, 'type': self.type_bdg,
                 'nbre_chp':Chapitre.query.filter_by(id_bdg=self.id_bdg).count()})


class Chapitre(MyModel):
    __tablename__ = 'chapitre_prev'
    __bind_key__ = 'prevision'
    id_chp = db.Column('ID_CHP', db.INTEGER, primary_key=True, autoincrement=True)
    id_bdg = db.Column(db.Integer, db.ForeignKey('budget_prev.ID_BDG'), nullable=False)
    id_usr = db.Column(db.Integer, db.ForeignKey('user.ID_USR'), nullable=False)
    nom_chp = db.Column('NOM_CHP', db.VARCHAR)
    solde_init_chp = db.Column('SOLDE_INIT_CHP', db.Numeric(45,2))
    solde_actuel_chp = db.Column('SOLDE_ACTUEL_CHP', db.Numeric(45,2))
    date_chp = db.Column('DATE_CHP', db.DATETIME)

    def __init__(self, nom,bdg,usr, solde_init, solde_act):
        self.nom_chp = nom
        self.solde_init_chp = solde_init
        self.solde_actuel_chp = solde_act
        self.id_bdg = bdg
        self.id_usr= usr        
        self.date_chp = strftime("%Y-%m-%d %H:%M:%S", gmtime())

    def __repr__(self):
        return "chapitre %s " % self.id_chp
        
    def commit(self):
        db.session.add(self)
        db.session.commit()
        pass

    def tograph(self):
        childs=[]
        chaps= Article.query.filter_by(id_chp=self.id_chp).all()
        for c in chaps:
            childs.append(c.tograph())

        return ({
            'name': self.nom_chp,
            'value': 1 if (Article.query.filter_by(id_chp=self.id_chp).count()==0) else Article.query.filter_by(id_chp=self.id_chp).count() ,
            'children': childs
            })

    def todict(self):
        usera = User.query.filter_by(id_usr=self.id_usr).first()
        budgeta = Budget.query.filter_by(id_bdg=self.id_bdg).first()
        return ({'id': self.id_chp, 'nom': self.nom_chp, 'date_c':self.date_chp,'solde_init': self.solde_init_chp,
                 'solde_actuel': self.solde_actuel_chp, 'budget': budgeta.todict(), 'user': usera.todict(),
                 'nbre_art':Article.query.filter_by(id_chp=self.id_chp).count()})

class Article(MyModel):
    __tablename__ = 'article_prev'
    __bind_key__ = 'prevision'
    id_art = db.Column('ID_ART', db.INTEGER, primary_key=True, autoincrement=True)
    id_usr = db.Column(db.Integer, db.ForeignKey('user.ID_USR'), nullable=False)
    id_chp = db.Column(db.Integer, db.ForeignKey('chapitre_prev.ID_CHP'), nullable=False)
    nom_art = db.Column('NOM_ART', db.VARCHAR)
    solde_init_art = db.Column('SOLDE_INIT_ART', db.Numeric(45,2))
    solde_actuel_art = db.Column('SOLDE_ACTUEL_ART', db.Numeric(45,2))
    date_art = db.Column('DATE_ART', db.DATETIME)

    def __init__(self, nom,chp,usr, solde_init, solde_act):
        self.nom_art = nom
        self.solde_init_art = solde_init
        self.solde_actuel_art = solde_act
        self.id_chp = chp
        self.id_usr= usr    
        self.date_art = strftime("%Y-%m-%d %H:%M:%S", localtime())

    def __repr__(self):
        return "article %s " % self.id_art
        
    def commit(self):
        db.session.add(self)
        db.session.commit()
        pass
    
    def tograph(self):
        childs=[]
        chaps= Prevision.query.filter_by(id_art=self.id_art).all()
        for c in chaps:
            childs.append(c.tograph())

        return ({
            'name': self.nom_art,
            'value': 1 if (Prevision.query.filter_by(id_art=self.id_art).count()==0) else Prevision.query.filter_by(id_art=self.id_art).count() ,
            'children': childs
            })

    def todict(self):
        usera = User.query.filter_by(id_usr=self.id_usr).first()
        chapitre = Chapitre.query.filter_by(id_chp=self.id_chp).first()
        return ({'id': self.id_art, 'nom': self.nom_art, 'date_c':self.date_art,'solde_init': self.solde_init_art,
                 'solde_actuel': self.solde_actuel_art, 'chapitre': chapitre.todict(), 'user': usera.todict(),
                 'nbre_prev':Prevision.query.filter_by(id_art=self.id_art).count()})


class Prevision(MyModel):
    __tablename__ = 'prevision'
    __bind_key__ = 'prevision'
    id_prev = db.Column('ID_PREV', db.INTEGER, primary_key=True, autoincrement=True)
    id_usr = db.Column(db.Integer, db.ForeignKey('user.ID_USR'), nullable=False)
    id_art = db.Column(db.Integer, db.ForeignKey('article_prev.ID_ART'), nullable=False)
    nom_prev = db.Column('NOM_PREV', db.VARCHAR)
    solde_prev = db.Column('SOLDE_PREV', db.Numeric(45,2))
    date_prev = db.Column('DATE_PREV', db.DATETIME)

    def __init__(self, nom,art,usr, solde):
        self.nom_prev = nom
        self.solde_prev = solde
        self.id_art = art
        self.id_usr= usr        
        self.date_prev = strftime("%Y-%m-%d %H:%M:%S", localtime())

    def __repr__(self):
        return "prevision %s " % self.id_prev
        
    def commit(self):
        db.session.add(self)
        db.session.commit()
        pass
    def tograph(self):
        return ({
            'name': self.nom_prev,
            'value':1
            })
    def todict(self):
        usera = User.query.filter_by(id_usr=self.id_usr).first()
        article = Article.query.filter_by(id_art=self.id_art).first()
        return ({'id': self.id_prev, 'nom': self.nom_prev, 'date_c':self.date_prev,
                 'solde_actuel': self.solde_prev, 'article': article.todict(), 'user': usera.todict()})


class Logs(MyModel):
    __tablename__ = 'logs'
    id_log = db.Column('ID_LOG', db.INTEGER, primary_key=True, autoincrement=True)
    date_log = db.Column('DATE_LOG', db.DATE)
    action_log = db.Column('ACTION_LOG', db.VARCHAR)
    user_id_usr = db.Column('ID_USR',db.Integer, db.ForeignKey('user.ID_USR'))

    def __init__(self, action, cpt):
        self.action_log = action
        self.user_id_usr = cpt.id_usr
        self.date_log = strftime("%Y-%m-%d %H:%M:%S", localtime())

    def commit(self):
        db.session.add(self)
        db.session.commit()
        pass