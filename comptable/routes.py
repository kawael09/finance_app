# coding=utf-8
import re
from time import strptime
import os
from flask import Blueprint, abort, render_template, redirect, request,make_response, jsonify,send_from_directory
from flask_login import login_required, logout_user, current_user
from jinja2 import TemplateNotFound
from manage import requires_roles, bcrypt
from models import db,Chapitre,Budget,Logs,Article,Prevision
import pdfkit
from datetime import datetime
from conf.config import Config
from werkzeug.utils import secure_filename
import flask_excel as excel
from sqlalchemy import and_,or_

comptable = Blueprint('comptable', __name__, template_folder='templates')
str = ''

no_valids_app = 0

@comptable.route('/home')
@login_required
@requires_roles('comptable')
def home():
    try:
        return render_template('comptable_home.html')
    except TemplateNotFound as ex:
        abort(404)



@comptable.route('/add_chap', methods=['GET','POST'])
@login_required
@requires_roles('comptable')
def add_chapiter():
    try:
        if request.method == 'POST':
            if(float(request.form['solde_init_chp'])>0):
                chapitre = Chapitre.query.filter(or_(Chapitre.nom_chp==request.form['nom_chp'],Chapitre.id_chp==request.form['nom_chp'])).first()
                if chapitre is None:
                    budget = Budget.query.filter_by(id_bdg=request.form['nom_bdg']).first()
                    if(budget.solde_actuel_bdg < float(request.form['solde_init_chp'])):
                        return jsonify({"message": "Ce solde initial est trop grand pour le budget selectionné  !! "})
                    else:    
                        if(budget.solde_actuel_bdg - float(request.form['solde_init_chp'])<=0):
                            return jsonify({"message": "Ce budget n'est pas suffisant pour ce solde initial !! "})
                        else:    
                            chapitre = Chapitre(request.form['nom_chp'],request.form['nom_bdg'],current_user.id_usr,float(request.form['solde_init_chp']),float(request.form['solde_init_chp']))
                            chapitre.commit()
                            data_cpt = dict()
                            data_cpt.update({"solde_actuel_bdg": budget.solde_actuel_bdg - float(request.form['solde_init_chp'])})
                            Budget.query.filter_by(id_bdg=request.form['nom_bdg']).update(data_cpt)
                            db.session.commit()
                            new_log = Logs('a créer le chapitre n°= '+chapitre.id_chp.__str__(), current_user)
                            new_log.commit()
                            return jsonify({"message": "success"})
                else:
                    return jsonify({"message": "Ce chapitre existe déja!! "})
            else:
                return jsonify({"message": "Le solde initial doit être plus grand que 0 !! "})
        else:
            return render_template('comptable_add_chap.html')
    except TemplateNotFound as ex:
        abort(404)

@comptable.route('/add_art', methods=['GET','POST'])
@login_required
@requires_roles('comptable')
def add_article():
    try:
        if request.method == 'POST':
            if(float(request.form['solde_init_art'])>0):
                article = Article.query.filter(or_(Article.nom_art==request.form['nom_art'],
                                Article.id_art==request.form['nom_art'])).first()
                if article is None:
                    chapitre = Chapitre.query.filter_by(id_chp=request.form['nom_chp']).first()
                    if(chapitre.solde_actuel_chp < float(request.form['solde_init_art'])):
                        return jsonify({"message": "Ce solde initial est trop grand pour le chapitre selectionné  !! "})
                    else:

                        if(chapitre.solde_actuel_chp - float(request.form['solde_init_art'])<=0):
                            return jsonify({"message": "Ce chapitre n'est pas suffisant pour ce solde initial !! "})
                        else:    
                            article = Article(request.form['nom_art'],request.form['nom_chp'],
                            current_user.id_usr,float(request.form['solde_init_art']),
                            float(request.form['solde_init_art']))
                            article.commit()
                            data_cpt = dict()
                            data_cpt.update({"solde_actuel_chp": chapitre.solde_actuel_chp - float(request.form['solde_init_art'])})
                            Chapitre.query.filter_by(id_chp=request.form['nom_chp']).update(data_cpt)
                            db.session.commit()
                            new_log = Logs('a créer l\'article n°= '+article.id_art.__str__(), current_user)
                            new_log.commit()
                            return jsonify({"message": "success"})
                else:
                    return jsonify({"message": "Cet Article existe déja!! "})
            else:
                return jsonify({"message": "Le solde initial doit être plus grand que 0 !! "})
        else:
            return render_template('comptable_add_art.html')
    except TemplateNotFound as ex:
        abort(404)

@comptable.route('/add_prev', methods=['GET','POST'])
@login_required
@requires_roles('comptable')
def add_prevision():
    try:
        if request.method == 'POST':
            if(float(request.form['solde_prev'])>0):
                article = Prevision.query.filter(or_(Prevision.nom_prev==request.form['nom_prev'],
                                Prevision.id_prev==request.form['nom_prev'])).first()
                if article is None:
                    chapitre = Article.query.filter_by(id_art=request.form['nom_art']).first()
                    if(chapitre.solde_actuel_art < float(request.form['solde_prev'])):
                        return jsonify({"message": "Ce solde est trop grand pour l'article selectionné  !! "})
                    else:

                        if(chapitre.solde_actuel_art - float(request.form['solde_prev'])<=0):
                            return jsonify({"message": "Cet article n'est pas suffisant pour ce solde !! "})
                        else:    
                            article = Prevision(request.form['nom_prev'],request.form['nom_art'],
                            current_user.id_usr,float(request.form['solde_prev']))
                            article.commit()
                            data_cpt = dict()
                            data_cpt.update({"solde_actuel_art": chapitre.solde_actuel_art - float(request.form['solde_prev'])})
                            Article.query.filter_by(id_art=request.form['nom_art']).update(data_cpt)
                            db.session.commit()
                            new_log = Logs('a créer la prévision n°= '+article.id_prev.__str__(), current_user)
                            new_log.commit()
                            return jsonify({"message": "success"})
                else:
                    return jsonify({"message": "Cette prévision existe déja!! "})
            else:
                return jsonify({"message": "Le solde initial doit être plus grand que 0 !! "})
        else:
            return render_template('comptable_add_prev.html')
    except TemplateNotFound as ex:
        abort(404)
# todo:   add prevision , list prevision
@comptable.route('/list_prev', methods=['GET','POST'])
@login_required
@requires_roles('comptable')
def list_prevision():
    try:
        list_prev=[]
        if request.method == 'POST':
            pattern_numb = re.compile("^([0-9]*)$")
            if(pattern_numb.match(request.form['nom_art'])):
                list_prev = Prevision.query.filter_by(id_usr=current_user.id_usr,
                id_art=request.form['nom_art']).all()
        else:
            list_prev = Prevision.query.filter_by(id_usr=current_user.id_usr).all()
        data_prevs= []
        for c in list_prev:
            data_prevs.append(c.todict())
        return render_template('comptable_list_prev.html',arts=data_prevs)
    except TemplateNotFound as ex:
        abort(404)

@comptable.route('/list_art', methods=['GET','POST'])
@login_required
@requires_roles('comptable')
def list_article():
    try:
        list_chap=[]
        if request.method == 'POST':
            pattern_numb = re.compile("^([0-9]*)$")
            if(pattern_numb.match(request.form['nom_chp'])):
                list_chap = Article.query.filter_by(id_usr=current_user.id_usr,
                id_chp=request.form['nom_chp']).all()
        else:
            list_chap = Article.query.filter_by(id_usr=current_user.id_usr).all()
        data_chaps= []
        for c in list_chap:
            data_chaps.append(c.todict())
        return render_template('comptable_list_art.html',arts=data_chaps)
    except TemplateNotFound as ex:
        abort(404)

@comptable.route('/list_chap', methods=['GET','POST'])
@login_required
@requires_roles('comptable')
def list_chapiter():
    try:
        list_chap=[]
        if request.method == 'POST':
            pattern_numb = re.compile("^([0-9]*)$")
            if(pattern_numb.match(request.form['nom_bdg'])):
                list_chap = Chapitre.query.filter_by(id_usr=current_user.id_usr,
                id_bdg=request.form['nom_bdg']).all()
        else:
            list_chap = Chapitre.query.filter_by(id_usr=current_user.id_usr).all()
        data_chaps= []
        for c in list_chap:
            data_chaps.append(c.todict())
        return render_template('comptable_list_chap.html',chaps=data_chaps)
    except TemplateNotFound as ex:
        abort(404)


# ----------------- JSON --------------------#
@comptable.route("/check_budgets", methods=['POST'])
@login_required
@requires_roles('comptable')
def check_budget():
    if request.method == 'POST':
        # pattern_text = re.compile("^([A-Za-z\ \,\-\_]*)$")
        # pattern_mat = re.compile("^([0-9]*)([\,]*[0-9]{,10})?$")
        get_app2 = Budget.query.filter(Budget.nom_bdg.like('%'+request.form['intitule']+'%')).all()
        data = []
        for app in get_app2:
            data.append({"value":app.id_bdg,"label":app.nom_bdg})
        if(len(data)>0):
            return jsonify({"code": "ok", "message": "success","data":data})
        else:
            return jsonify({"code": "fail", "message": "Nouveau chapitre!","data":[] })

@comptable.route("/check_arts", methods=['POST'])
@login_required
@requires_roles('comptable')
def check_art():
    if request.method == 'POST':
        # pattern_text = re.compile("^([A-Za-z\ \,\-\_]*)$")
        # pattern_mat = re.compile("^([0-9]*)([\,]*[0-9]{,10})?$")
        get_app2 = Article.query.filter(Article.nom_art.like('%'+request.form['intitule']+'%')).all()
        data = []
        for app in get_app2:
            data.append({"value":app.id_art,"label":app.nom_art})
        if(len(data)>0):
            return jsonify({"code": "ok", "message": "success","data":data})
        else:
            return jsonify({"code": "fail", "message": "Nouveau chapitre!","data":[] })

@comptable.route("/check_chaps", methods=['POST'])
@login_required
@requires_roles('comptable')
def check_chap():
    if request.method == 'POST':
        # pattern_text = re.compile("^([A-Za-z\ \,\-\_]*)$")
        # pattern_mat = re.compile("^([0-9]*)([\,]*[0-9]{,10})?$")
        get_app2 = Chapitre.query.filter(Chapitre.nom_chp.like('%'+request.form['intitule']+'%')).all()
        data = []
        for app in get_app2:
            data.append({"value":app.id_chp,"label":app.nom_chp})
        if(len(data)>0):
            return jsonify({"code": "ok", "message": "success","data":data})
        else:
            return jsonify({"code": "fail", "message": "Nouveau chapitre!","data":[] })
@comptable.route("/check_prevs", methods=['POST'])
@login_required
@requires_roles('comptable')
def check_prev():
    if request.method == 'POST':
        # pattern_text = re.compile("^([A-Za-z\ \,\-\_]*)$")
        # pattern_mat = re.compile("^([0-9]*)([\,]*[0-9]{,10})?$")
        get_app2 = Prevision.query.filter(Prevision.nom_prev.like('%'+request.form['intitule']+'%')).all()
        data = []
        for app in get_app2:
            data.append({"value":app.id_prev,"label":app.nom_prev})
        if(len(data)>0):
            return jsonify({"code": "ok", "message": "success","data":data})
        else:
            return jsonify({"code": "fail", "message": "Nouveau chapitre!","data":[] })

@comptable.route("/exist_chap", methods=['POST'])
@login_required
@requires_roles('comptable')
def exist_chap():
    if request.method == 'POST':
        pattern_text = re.compile("^([A-Za-z0-9\ \_\-\.\']*)$")
        pattern_numb = re.compile("^([0-9]*)$")
        if(pattern_numb.match(request.form['intitule'])):
            get_app2 = Chapitre.query.filter_by(id_chp=request.form['intitule']).first()
            if (get_app2):
                return jsonify({"code": "fail", "message": "Vous avez déja insérer ce chapitre !!"})
            else:
                return jsonify({"code": "new", "message": "Nouveau chapitre!!"})
        elif(pattern_text.match(request.form['intitule'])):
            get_app2 = Chapitre.query.filter_by(nom_chp=request.form['intitule']).first()
            if (get_app2 is None):
                return jsonify({"code": "new", "message": "Nouveau chapitre!!"})
            else:
                return jsonify({"code": "fail", "message": "Vous avez déja insérer ce chapitre !!"})

@comptable.route("/exist_art", methods=['POST'])
@login_required
@requires_roles('comptable')
def exist_art():
    if request.method == 'POST':
        pattern_text = re.compile("^([A-Za-z0-9\ \_\-\.\']*)$")
        pattern_numb = re.compile("^([0-9]*)$")
        if(pattern_numb.match(request.form['intitule'])):
            get_app2 = Article.query.filter_by(id_art=request.form['intitule']).first()
            if (get_app2):
                return jsonify({"code": "fail", "message": "Vous avez déja insérer ce chapitre !!"})
            else:
                return jsonify({"code": "new", "message": "Nouveau chapitre!!"})
        elif(pattern_text.match(request.form['intitule'])):
            get_app2 = Article.query.filter_by(nom_art=request.form['intitule']).first()
            if (get_app2 is None):
                return jsonify({"code": "new", "message": "Nouveau chapitre!!"})
            else:
                return jsonify({"code": "fail", "message": "Vous avez déja insérer ce chapitre !!"})

@comptable.route("/exist_prev", methods=['POST'])
@login_required
@requires_roles('comptable')
def exist_prev():
    if request.method == 'POST':
        pattern_text = re.compile("^([A-Za-z0-9\ \_\-\.\']*)$")
        pattern_numb = re.compile("^([0-9]*)$")
        if(pattern_numb.match(request.form['intitule'])):
            get_app2 = Prevision.query.filter_by(id_prev=request.form['intitule']).first()
            if (get_app2):
                return jsonify({"code": "fail", "message": "Vous avez déja insérer cette prévision !!"})
            else:
                return jsonify({"code": "new", "message": "Nouvelle prévision!!"})
        elif(pattern_text.match(request.form['intitule'])):
            get_app2 = Prevision.query.filter_by(nom_prev=request.form['intitule']).first()
            if (get_app2 is None):
                return jsonify({"code": "new", "message": "Nouvelle prévision!!"})
            else:
                return jsonify({"code": "fail", "message": "Vous avez déja insérer cette prévision !!"})

@comptable.route("/get_budget", methods=['POST'])
@login_required
@requires_roles('comptable')
def get_budget():
    if request.method == 'POST':
        pattern_text = re.compile("^([0-9]*)$")
        if(pattern_text.match(request.form['budget'])):
            get_app2 = Budget.query.filter_by(id_bdg=request.form['budget']).first()
            if (get_app2):
                return jsonify({"code": "ok", "budget": get_app2.solde_actuel_bdg})
            else:
                return jsonify({"code": "fail", "message": "Pas de Budget!!"})
        else:
            get_app2 = Budget.query.filter_by(nom_bdg=request.form['budget']).first()
            if (get_app2 is None):
                return jsonify({"code": "fail", "message": "Pas de Budget!!"})
            else:
                return jsonify({"code": "ok",  "budget": get_app2.solde_actuel_bdg})

@comptable.route("/get_chap", methods=['POST'])
@login_required
@requires_roles('comptable')
def get_chap():
    if request.method == 'POST':
        pattern_text = re.compile("^([0-9]*)$")
        if(pattern_text.match(request.form['chap'])):
            get_app2 = Chapitre.query.filter_by(id_chp=request.form['chap']).first()
            if (get_app2):
                return jsonify({"code": "ok", "budget": get_app2.solde_actuel_chp})
            else:
                return jsonify({"code": "fail", "message": "Pas de chapitre!!"})
        else:
            get_app2 = Chapitre.query.filter_by(nom_chp=request.form['chap']).first()
            if (get_app2 is None):
                return jsonify({"code": "fail", "message": "Pas de chapitre!!"})
            else:
                return jsonify({"code": "ok",  "budget": get_app2.solde_actuel_chp})
@comptable.route("/get_art", methods=['POST'])
@login_required
@requires_roles('comptable')
def get_art():
    if request.method == 'POST':
        pattern_text = re.compile("^([0-9]*)$")
        if(pattern_text.match(request.form['art'])):
            get_app2 = Article.query.filter_by(id_art=request.form['art']).first()
            if (get_app2):
                return jsonify({"code": "ok", "budget": get_app2.solde_actuel_art})
            else:
                return jsonify({"code": "fail", "message": "Pas d'artcile!!"})
        else:
            get_app2 = Article.query.filter_by(nom_art=request.form['art']).first()
            if (get_app2 is None):
                return jsonify({"code": "fail", "message": "Pas d'artcile!!"})
            else:
                return jsonify({"code": "ok",  "budget": get_app2.solde_actuel_art})

@comptable.route("/get_data", methods=['POST'])
@login_required
@requires_roles('comptable')
def get_data():
    if request.method == 'POST':
        get_app2 = Budget.query.all()
        data_chaps=[]
        for i in get_app2:
            data_chaps.append(i.tograph())
        if (get_app2 is None):
            return jsonify({"code": "fail", "data": data_chaps})
        else:
            return jsonify({"code": "ok",  "data": data_chaps})


# @comptable.route('/print_articles', methods=['GET'])
# @login_required
# @requires_roles('comptable')
# def getpdf():
#     global no_valids_app
    
#     try:
#         if current_user.is_authenticated:
#             rdv = Article.query.all()
#             data = []
#             for r in rdv:
#                 data.append(r.todict_min())
#             if (rdv):
#                 page = render_template('user_print_arts.html', rdv=data, now=datetime.today().strftime('%d/%m/%Y'))
#                 options = {
#                     'quiet': '',
#                     'encoding': "UTF-8",
#                     "enable-local-file-access": None
#                 }
#                 file_name=current_user.pseudo_cpt +'.'+ datetime.today().strftime('%d.%m.%Y')
#                 conf = pdfkit.configuration(wkhtmltopdf=Config.WKHTMLTOPDF_BIN_PATH)
#                 pdf = pdfkit.from_string(page,False,
#                 #  os.path.dirname(os.path.realpath(__file__))+'/pdfs/'+file_name +'.pdf',
#                 css= os.path.dirname(os.path.realpath(__file__))+'/../static/plugins/bootstrap/css/bootstrap.min.css' , options=options,
#                 configuration=conf)
#                 response = make_response(pdf)
#                 response.headers['Content-Type'] = 'application/pdf'
#                 response.headers['Content-Disposition'] = 'inline; filename='+file_name+'.pdf'
                
#                 return response
#                 # return send_from_directory(directory=os.path.dirname(os.path.realpath(__file__))+'/pdfs', filename= file_name+'.pdf', as_attachment=True)
#                 # os.remove(os.path.dirname(os.path.realpath(__file__))+'/pdfs/'+file_name+'.pdf')
#             else:
#                 return redirect("/user/list_article")
#     except TemplateNotFound:
#         abort(404)

# @comptable.route('/export_excel.csv', methods=['GET'])
# @login_required
# @requires_roles('comptable')
# def getexcel():
#     global no_valids_app
    
#     try:
#         if current_user.is_authenticated:
#             rdv = Article.query.all()
#             data = [['code','métier','famille','sous famille','nom']]
#             for r in rdv:
#                 data.append(r.todict_excel())
#             if (rdv):
#                 return excel.make_response_from_array(data,"csv")
#             else:
#                 return redirect("/user/list_article")
#     except TemplateNotFound:
#         abort(404)