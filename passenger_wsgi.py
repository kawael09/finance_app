#!/usr/bin/env python

"""
Passenger WSGI script for hosting on Dreamhost. Assumes a virtualenv
configured at ~/env/[appname].
"""

import sys
import os

INTERP = os.path.expanduser("C:\Anaconda3\envs\doctor")
if sys.executable != INTERP:
    os.execl(INTERP, INTERP, *sys.argv)

sys.path.append(os.getcwd())

from manage import app as application

'''
sys.path.insert(0, os.path.dirname(__file__))


def application(environ, start_response):
    start_response('200 OK', [('Content-Type', 'text/plain')])
    message = 'It works!\n'
    version = 'Python %s\n' % sys.version.split()[0]
    response = '\n'.join([message, version])
    return [response.encode()]
'''
