
$('#reloader').click(function (e) {
    e.preventDefault();
    $.ajax({
        url: "/regene_captcha",
        method: "POST",
        timeout: 6000,
        success: function (data) {
            $('.captcha').html('<img src="data:image/jpg;base64,' + data.captcha + '" />');
        },
        error: function () {
            toastr.error("Désolé, aucun résultat trouvé.");
        }
    });
});

$("#login").on("submit", function (e) {
    e.preventDefault();
    // toastr.options["positionClass"]= "toast-bottom-right";
    // toastr.options["progressBar"]= true;
    $('.dimmer').addClass('active');
    var u = $(this).find('input[name="username"]').val();
    var p = $(this).find('input[name="password"]').val();
    var c = $(this).find('input[name="captcha"]').val();
    document.getElementById('alert').className="card-alert alert mb-0";
    $.ajax({
        url: "/login",
        method: "POST",
        data: {"username": u, "password": p, "captcha": c},
        timeout: 6000,
        success: function (data) {
            $('.dimmer').removeClass('active');
            if (data.message == "success") {
                $('#alert').addClass("alert-success");
                $('#alert').text("Authentification réussite.");
                setTimeout(function () {
                    window.location = '/' + data.root + '/home';
                }, 1500);
            } else {
                $('.captcha').html('<img src="data:image/jpg;base64,' + data.captcha + '" />');
                
                $('#alert').addClass("alert-warning");
                $('#alert').text(data.message);
            }
        },
        error: function () {
            $('.dimmer').removeClass('active');
            $('.captcha').html('<img src="data:image/jpg;base64,' + data.captcha + '" />');
            $('#alert').addClass("alert-danger");
            $('#alert').text("Désolé, aucun résultat trouvé.");
        }

    });
});