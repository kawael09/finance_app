
require(['echarts', 'jquery'], function(echarts, $) {

	var chartDom = document.getElementById('main');
	var myChart = echarts.init(chartDom);
	
	var option;
	$.post('/comptable/get_data').done(function (data) {
		console.log(data.data);
		option = {
			series: {
				type: 'sunburst',
				data: data.data,
				radius: [60, '90%'],
				itemStyle: {
					borderRadius: 2,
					borderWidth: 2
				},
				label: {
					show: true
				},
				emphasis: {
					focus: 'ancestor'
				},
		
				levels: [{}, {
					r0: '15%',
					r: '35%',
					itemStyle: {
						borderWidth: 2
					},
					label: {
						rotate: 'tangential'
					}
				}, {
					r0: '35%',
					r: '50%',
					label: {
						// align: 'center',
						rotate:  'tangential'
					}
				}, {
					r0: '50%',
					r: '65%',
					label: {
						rotate: 'tangential'
					},
					itemStyle: {
						borderWidth: 3
					}
				},
				{
					r0: '65%',
					r: '81%',
					label: {
					// 	position: 'outside',
					// 	padding: 3,
					// 	silent: false
						
					rotate: 'tangential'
					},
					itemStyle: {
						borderWidth: 3
					}
				}
			]
			}
		};
		
		option && myChart.setOption(option);
	});
	
});