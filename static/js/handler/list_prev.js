
require(['datatables', 'jquery'], function(datatable, $) {
    $('.datatable').DataTable();
});
require(['selectize', 'jquery'], function(selectize, $) {
    $('#form2').selectize({
        persist: false,
        valueField: 'value',
        labelField: 'label',
        searchField: 'label',
        options: [],
        create: false,
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: "/comptable/check_arts",
                method: "POST",
                dataType: 'json',
                data: {'intitule': query},
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res.data);
                }
            });
        }
    });
    
  });
  