$('#reloader').click(function (e) {
    e.preventDefault();
    $.ajax({
        url: "/regene_captcha",
        method: "POST",
        timeout: 6000,
        success: function (data) {
            $('.captcha').html('<img src="data:image/jpg;base64,' + data.captcha + '" />');
        }
    });

});
$('#multis').selectize({
    onChange: function (value) {
        $.ajax({
            url: '/get_structures',
            data: {'str': encodeURIComponent(value)},
            type: 'POST',
            error: function () {
                callback();
            },
            success: function (res) {
                $('#strs').parent().removeClass("d-none");
                $('#strs').selectize()[0].selectize.destroy();
                $('#strs').selectize({
                    create: true,
                    persist: false,
                    valueField: 'label',
                    labelField: 'label',
                    searchField: ['label'],
                    options: res.data
                });

            }
        });
    }
});
$("#register").on("submit", function (e) {
    e.preventDefault();
    var r = $(this).find('select[name="region"]').val();
    var s = $(this).find('select[name="structure"]').val();
    var u = $(this).find('input[name="username"]').val();
    var p = $(this).find('input[name="password"]').val();
    var e = $(this).find('input[name="conf_password"]').val();
    var c = $(this).find('input[name="captcha"]').val();
    document.getElementsByClassName('alert').className = 'alert';
    if (p == e) {
        $('#loader').removeClass('d-none');
        document.getElementsByClassName('alert').className='alert';
        $.ajax({
            url: "/register",
            method: "POST",
            data: {"username": u, "password": p, "structure": s, "region": r, "captcha": c},
            timeout: 6000,
            success: function (data) {
                $('#loader').addClass('d-none');
                if (data.message == "success") {

                    $('.alert').addClass('alert-success');
                    $('.alert').text("Inscription réussite, Votre compte sera prochainement validé .");
                    $('.alert').alert();
                    setTimeout(function () {
                        window.location.href = '/login';
                    }, 2000);
                } else {
                    $('.captcha').html('<img src="data:image/jpg;base64,' + data.captcha + '" />');
                    $('.alert').addClass('alert-warning');
                    $('.alert').text(data.message);
                    $('.alert').alert();
                }
            },
            error: function () {
                $('.captcha').html('<img src="data:image/jpg;base64,' + data.captcha + '" />');
                $('#loader').addClass('d-none');
                $('.alert').addClass('alert-error');
                $('.alert').text(data.message);
                $('.alert').alert();
            }

        });
    } else {
        alert("Veuillez verifier votre mot de passe!!");
    }
});
