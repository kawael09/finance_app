var budget=0;
require(['selectize', 'jquery'], function(selectize, $) {
    $('#form2').selectize({
        persist: false,
        valueField: 'value',
        labelField: 'label',
        searchField: 'label',
        options: [],
        create: false,
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: "/comptable/check_budgets",
                method: "POST",
                dataType: 'json',
                data: {'intitule': query},
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res.data);
                }
            });
        },
        onChange: function(query, callback){
            // selectizers[this.$input[0].name]=query;
            $.ajax({
                url: "/comptable/get_budget",
                method: "POST",
                dataType: 'json',
                data: {'budget': query},
                error: function() {
                    callback();
                },
                success: function(res) {
                    if(res.code=="ok"){
                        budget=res.budget;
                        $('.budget').val(budget);
                        require(['circle-progress'], function() {
                            $('.chart-circle').circleProgress({ value: (budget/budget) });
                        });
                        
                        $('.chart-circle-value').text((budget/budget)*100+'%');
                    }else{
                        if($('.feedback').hasClass('text-success')){
                            $('.feedback').removeClass("text-success");
                        }
                        $('.feedback').addClass("text-warning").html(res.message);
                    }
                }
            });
        }
    });
    $('.budget').on('change',function(e){
        $('.chart-circle').circleProgress({ value: (parseFloat($(this).val())/budget).toFixed(2) });
        $('.chart-circle-value').text(((parseFloat($(this).val())/budget)*100).toFixed(2)+'%');

    });
    $('#form3').selectize({
        persist: false,
        valueField: 'value',
        labelField: 'label',
        searchField: 'label',
        options: [],
        create: function(input) {
                return {
                    value: input,
                    label: input
                }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: "/comptable/check_chaps",
                method: "POST",
                dataType: 'json',
                data: {'intitule': query},
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res.data);
                }
            });
        },
        onChange: function(query, callback){
            // selectizers[this.$input[0].name]=query;
            $.ajax({
                url: "/comptable/exist_chap",
                method: "POST",
                dataType: 'json',
                data: {'intitule': query},
                error: function() {
                    callback();
                },
                success: function(res) {
                    if(res.code=="new"){
                        if($('.feedback').hasClass('text-warning')){
                            $('.feedback').removeClass("text-warning");
                        }
                        $('.feedback').addClass("text-success").html(res.message);
                        
                    }else{
                        if($('.feedback').hasClass('text-success')){
                            $('.feedback').removeClass("text-success");
                        }
                        $('.feedback').addClass("text-warning").html(res.message);
                        
                        
                    }
                }
            });
        }
    });

    $("#add_chap").on("submit", function (e) {
        e.preventDefault();
        $('.dimmer').addClass('active');
        var u = $('#form2').val();
        var p = $('#form3').val();
        var s = $(this).find('input[name="solde_init_chp"]').val();
        document.getElementById('alert').className="card-alert alert mb-0";
        $.ajax({
            url: "/comptable/add_chap",
            method: "POST",
            data: {"nom_bdg": u, "nom_chp": p, "solde_init_chp": s},
            timeout: 6000,
            success: function (data) {
                $('.dimmer').removeClass('active');
                if (data.message == "success") {
                    $('#alert').addClass("alert-success");
                    $('#alert').html('<i class="fe fe-check mr-2" aria-hidden="true"></i>Chapitre ajouté.');
                    // setTimeout(function () {
                    //     window.location = '/' + data.root + '/home';
                    // }, 2000);
                } else {
                    $('#alert').addClass("alert-warning");
                    $('#alert').text(data.message);
                }
            },
            error: function () {
                $('.dimmer').removeClass('active');
                $('#alert').addClass("alert-danger");
                $('#alert').html('<i class="fe fe-alert-triangle mr-2" aria-hidden="true"></i> Désolé, aucun résultat trouvé.');
            }
    
        });
    });
});