require.config({
	paths: {
		"eve-loader": "/static/plugins/raphael/raphael-loader/eve-loader", /*  for loading `eve` dependency */
		"raphael.core": "/static/plugins/raphael/raphael/raphael.core",
		"raphael.svg": "/static/plugins/raphael/raphael/raphael.svg",
		"raphael.vml": "/static/plugins/raphael/raphael/raphael.vml"
	},
	shim: {
		'raphael.core': {
			deps: ["eve-loader"]
		},
		'raphael.svg': {
			deps: ["raphael.core"]
		},
		'raphael.vml': {
			deps: ["raphael.core"]
		}
	}
});

define(
	["raphael.svg", "raphael.vml"],
	function () {}
);
