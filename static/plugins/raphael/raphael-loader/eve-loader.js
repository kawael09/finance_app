require.config({
	paths: {
		"eve": "/static/plugins/raphael/raphael/eve/eve"
	},
	shim: {
		'eve': {
			exports: "eve"
		}
	}
});

define(
	["eve"],
	function (eve) {
		this.eve = eve;
	}
);


