# Application web pour la gestion des prévisions budgetaires
Cette permet de créer les chapitres, Articles et prévisions de chaque CRSS.


## Les étapes a suivre pour déployer l'application:

 * 1 - installer [anaconda]() (gestionnaire des environnements des solution de Python)

 * 2 - creer un nouveau environnement 
```
 conda create --name finance python=3.5
```
 * 3 - Pour entrer dans le nouveau environnement
```
  activate finance
```

 * 4 - Pour importer les bibliothèques requis pour executer l'application
 ```
    pip install -r requirements.txt
 ```
 * 5 - cd /d chemin de l'application 
 
 ```
    cd /d D:/finance_app/
 ```

 * 6 - Pour executer l'application web

 ```
    python manage.py
 ```
 * 7 - Ouvrir le navigateur Web et saisir l'addresse http://localhost:80

 
## Notes

* L'accée à l'application
```
        username : compt1
        password : hello
```
* Il faut etre connecté à l'internet pour pouvoir télécharger les bibliothèques de python.*

 
## Développeur
 - Gmail [kawael09@gmail.com]()
