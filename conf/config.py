# coding=utf-8
import json
import os
class Config(object):
    DEBUG = True
    TESTING = False
    # DATABASE_URI = 'mysql://root@localhost/helpdesk'
    SECRET_KEY = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    UPLOAD_FOLDER = 'static/images/products/'
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif', 'svg'}
    SQLALCHEMY_DATABASE_URI = 'mysql://root:------@localhost/finance_camssp'
    SQLALCHEMY_BINDS = {
        'prevision': "mysql://root:------@localhost/finance_prevision"
    }
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    WKHTMLTOPDF_BIN_PATH= "C:/Program Files/wkhtmltopdf/bin/wkhtmltopdf.exe"
    
class Company(object):
    NOM_COMP = ''
    ADDRESS_COMP = ''
    DESC_COMP = ''
    PORTABLE_COMP = ''
    EMAIL_COMP = ''
    ALT_COMP = 36.4823009
    LONG_COMP = 2.8257632
    FB_COMP= ""
    TW_COMP = ""
    IST_COMP = ""
    def __init__(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        with open(dir_path+'/conf.json') as json_data_file:
            data = json.load(json_data_file)
        self.ADDRESS_COMP = data['company']['address']
        self.PORTABLE_COMP = data['company']['telephone']
        self.EMAIL_COMP = data['company']['email']
        self.ALT_COMP = data['company']['altitude']
        self.LONG_COMP = data['company']['longitude']
        self.NOM_COMP = data['company']['nom']
        self.DESC_COMP = data['company']['desc']
        self.FB_COMP = data['company']['facebook']
        self.TW_COMP = data['company']['twitter']
        self.IST_COMP = data['company']['instagram']
