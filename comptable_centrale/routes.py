# coding=utf-8
import re
from time import strptime
import os
from flask import Blueprint, abort, render_template, redirect, request,make_response, jsonify,send_from_directory
from flask_login import login_required, logout_user, current_user
from jinja2 import TemplateNotFound
from manage import requires_roles, bcrypt
from models import Article, Famille, Metier, SousFamille, db
import pdfkit
from datetime import datetime
from conf.config import Config
from werkzeug.utils import secure_filename
import flask_excel as excel
from sqlalchemy import and_,or_

admin = Blueprint('admin', __name__, template_folder='templates')
str = ''

no_valids_app = 0


@admin.route('/')
@login_required
@requires_roles('admin')
def home():
    try:
        return render_template('admin_home.html')
    except TemplateNotFound:
        abort(404)


@admin.route("/get_mets", methods=['POST'])
@login_required
@requires_roles('admin')
def get_mets():
    if request.method == 'POST':
        mets = Metier.query.all()
        data=[]
        for fam in mets:
            familles = Famille.query.filter_by(code_met= fam.code_met).all()
            data.append({'text':' '+fam.code_met+' - '+ fam.nom_met,
                        'icon': "fa fa-briefcase" })
            fs=[]
            for f in familles:
                sfamilles = SousFamille.query.filter_by(code_fam= f.code_fam).all()
                sfs=[]
                fs.append({'text':' '+f.code_fam +' - '+f.nom_fam,
                        'icon': "fa fa-bookmark"})
                for sf in sfamilles:
                    articles = Article.query.filter_by(code_sfam= sf.code_sfam).all()
                    arts=[]
                    sfs.append({'text':' '+sf.code_sfam +' - '+sf.nom_sfam,
                        'icon': "fa fa-book-open"})
                    for art in articles:
                        arts.append({'text':' '+art.code_art +' - '+art.nom_art,
                        'icon': "fa fa-box-open",
                        'image': art.img_art,
                        'designation': art.designation_art,
                        'class': 'article',
                        'metier':fam.nom_met,
                        'fam':f.nom_fam,
                        'sfam':sf.nom_sfam,
                        
                        'code':fam.code_met+f.code_fam+sf.code_sfam+art.code_art})
                    
                    sfs[len(sfs)-1]["nodes"]=arts
                fs[len(fs)-1]["nodes"]=sfs
            data[len(data)-1]["nodes"]=fs

        if data is not None:
            return jsonify({"code": "ok", "message": "success", "data": data})
        else:
            return jsonify({"code": "fail", "message": "Cette application existe déja!!!"})

@admin.route("/get_fams", methods=['POST'])
@login_required
@requires_roles('admin')
def get_fams():
    if request.method == 'POST':
        if(request.form['str']):
            sfams = Famille.query.filter_by(code_met=request.form['str']).all()
        else:
            sfams = Famille.query.all()
        data=[]
        for fam in sfams:
            data.append({'label':fam.nom_fam, 'value': fam.code_fam})
        if data is not None:
            return jsonify({"code": "ok", "message": "success", "data": data})
        else:
            return jsonify({"code": "fail", "message": "Cette application existe déja!!!"})


@admin.route("/get_sfams", methods=['POST'])
@login_required
@requires_roles('admin')
def get_sfams():
    if request.method == 'POST':
        if(request.form['str']):
            sfams = SousFamille.query.filter_by(code_fam=request.form['str'],code_met=request.form['str2']).all()
        else:
            sfams = SousFamille.query.all()
        data=[]
        for fam in sfams:
            data.append({'label':fam.nom_sfam, 'value': fam.code_sfam})
        if data is not None:
            return jsonify({"code": "ok", "message": "success", "data": data})
        else:
            return jsonify({"code": "fail", "message": "Cette application existe déja!!!"})


@admin.route("/get_arts", methods=['POST'])
@login_required
@requires_roles('admin')
def get_arts():
    if request.method == 'POST':
        if(request.form['str']):
            sfams = Article.query.filter_by(code_sfam=request.form['str'],code_fam=request.form['str2'],code_met=request.form['str3']).all()
            
        else:
            sfams = Article.query.all()
        maxy = Article.query.filter_by(code_sfam=request.form['str'],code_fam=request.form['str2'],code_met=request.form['str3']).count()
        data=[]
        for fam in sfams:
            data.append({'label':fam.nom_art, 'value': fam.code_art})
        
        if data is not None:
            return jsonify({"code": "ok", "message": "success", "data": data , "max":maxy})
        else:
            return jsonify({"code": "fail", "message": "aucun article trouvé!!!"})



@admin.route("/search_arts", methods=['POST'])
@login_required
@requires_roles('admin')
def search_arts():
    if request.method == 'POST':
        if(request.form['champ']=="code"):
            sfams = Article.query.filter(or_(Article.code_sfam.like(request.form['search']+'%'),
                                Article.code_fam.like(request.form['search']+'%'),
                                Article.code_met.like(request.form['search']+'%'),
                                Article.code_art.like(request.form['search']+'%'))).all()
        if(request.form['champ']=="nom"):
            sfams = Article.query.filter(Article.nom_art.like(request.form['search']+'%')).all()
        
        if(request.form['champ']=="designation"):
            sfams = Article.query.filter(Article.designation_art.like(request.form['search']+'%')).all()
        
        data=[]
        if(request.form['champ']):
            for fam in sfams:
                data.append(fam.todict())
        
        if data is not None:
            return jsonify({"code": "ok", "message": "success", "data": data })
        else:
            return jsonify({"code": "fail", "message": "aucun article trouvé!!"})

@admin.route("/get_art", methods=['POST'])
@login_required
@requires_roles('admin')
def get_art():
    if request.method == 'POST':
        if(request.form['str']):
            sfams = Article.query.filter(and_(Article.code_met==request.form['str'],Article.code_fam==request.form['str2'],
            Article.code_sfam==request.form['str3'],Article.code_art==request.form['str4'])).first()
        
        if sfams is not None:
            return jsonify({"code": "ok", "message": "success", "data": sfams.todict()})
        else:
            return jsonify({"code": "fail", "message": "Cette application existe déja!!!"})


@admin.route("/list_article", methods=['GET','POST'])
@login_required
@requires_roles('admin')
def liste_arts():
    if request.method == 'GET':
        mets = Metier.query.all()
        data2 = []
        for m in mets:
            data2.append(m.todict())
        arts = Article.query.all()
        data = []
        for m in arts:
            data.append(m.todict())
        return render_template('liste_article.html',arts=data,mets=data2)
    else:
        if request.method == 'POST':
            pattern_text = re.compile("^([0-9A-Za-z\ \,\-\_]*)$")
            if (pattern_text.match(request.form['nom_sfam']) and pattern_text.match(request.form['code_sfam']) and pattern_text.match(request.form['metier'])
                     and pattern_text.match(request.form['famille']) and pattern_text.match(request.form['design_sfam'])):
                new_sfam = SousFamille.query.filter_by(code_met=request.form['metier'],code_fam=request.form['famille'],code_sfam=request.form['code_sfam']).first()
                if(new_sfam):
                    new_sfam.nom_sfam=request.form['nom_sfam']
                    new_sfam.designation_sfam=request.form['design_sfam']
                else:
                    new_sfam = SousFamille( request.form['code_sfam'] ,request.form['nom_sfam'] ,request.form['design_sfam'],request.form['metier'],request.form['famille'])
                
                new_sfam.commit()
                db.session.commit()
                return jsonify({"code": "ok", "message": "success"})
            else:
                return jsonify({"code": "fail", "message": "Veuillez respecter les combinaisons des champs."})

@admin.route("/add_article", methods=['GET','POST'])
@login_required
@requires_roles('admin')
def add_article():
    fams = Metier.query.all()
    data=[]
    for fam in fams:
        data.append(fam.todict())
    if request.method == 'GET':
        return render_template('add_article.html',mets=data)
    else:
        if request.method == 'POST':
            # print(request.url_rule)
            pattern_text = re.compile("^([0-9A-Za-z\/\:\(\)' \ \,\-\_]*)$")
            if (pattern_text.match(request.form['nom_art']) and pattern_text.match(request.form['code_art'])
                    and pattern_text.match(request.form['design_art']) and pattern_text.match(request.form['metier'])
                    and pattern_text.match(request.form['famille']) and pattern_text.match(request.form['sous_famille']) ):
                
                old_art = Article.query.filter_by(code_art=request.form['code_art'],code_met=request.form['metier'],
                                    code_fam=request.form['famille'],code_sfam=request.form['sous_famille']).first()
                if(not old_art):
                    img = request.files['file']
                    srtfile=''
                    if img and allowed_file(img.filename):
                        filename= secure_filename(img.filename)
                        srtfile =os.path.join(Config.UPLOAD_FOLDER, request.form['metier']+'.'+request.form['famille']+'.'+
                                request.form['sous_famille']+'.'+request.form['code_art']+'.'+filename.split('.')[len(filename.split('.'))-1])
                        img.save(srtfile)
                    new_met = Article( request.form['code_art'] ,request.form['nom_art'] ,request.form['design_art'],srtfile,
                        request.form['metier'],request.form['famille'],request.form['sous_famille'])
                    new_met.commit()
                    db.session.commit()
                    return render_template('add_article.html',fams=data,code="ok",message="Article ajouté avec succéé !")
                else:
                    return render_template('add_article.html',fams=data,code="fail",message="Article éxiste déja !")
                    
            else:
                return render_template('add_article.html',code="fail",message="Veuillez respecter les combinaisons des champs.")

@admin.route("/update_article", methods=['POST'])
@login_required
@requires_roles('admin')
def update_article():
    if request.method == 'POST':
            # print(request.url_rule)
        pattern_text = re.compile("^([0-9A-Za-z\/\:\(\)' \ \,\-\_]*)$")
        if (pattern_text.match(request.form['nom_art']) and pattern_text.match(request.form['code_art'])
                and pattern_text.match(request.form['design_art']) and pattern_text.match(request.form['metier'])
                and pattern_text.match(request.form['famille']) and pattern_text.match(request.form['sous_famille']) ):
            
            old_art = Article.query.filter_by(code_art=request.form['code_art'],code_met=request.form['metier'],
                                code_fam=request.form['famille'],code_sfam=request.form['sous_famille']).first()
            if(old_art):
                img = request.files['file']
                srtfile=''
                if img and allowed_file(img.filename):
                    filename= secure_filename(img.filename)
                    srtfile =os.path.join(Config.UPLOAD_FOLDER, request.form['metier']+'.'+request.form['famille']+'.'+
                            request.form['sous_famille']+'.'+request.form['code_art']+'.'+filename.split('.')[len(filename.split('.'))-1])
                    img.save(srtfile)
                
                new_met = Article.query.filter_by(code_art=request.form['code_art'],code_met=request.form['metier'],
                                    code_fam=request.form['famille'],code_sfam=request.form['sous_famille']).update(({"nom_art":request.form['nom_art'] ,"designation_art":request.form['design_art'],"img_art":srtfile}))
                db.session.commit()
                return redirect('/admin/list_article')
            else:
                return redirect('/admin/list_article')

        else:
            return redirect('/admin/list_article')

@admin.route("/add_met", methods=['GET','POST'])
@login_required
@requires_roles('admin')
def add_met():
    if request.method == 'GET':
        mets = Metier.query.all()
        data = []
        for m in mets:
            data.append(m.todict())
        return render_template('add_met.html',mets=data)
    else:
        if request.method == 'POST':
            pattern_text = re.compile("^([0-9A-Za-zéèà \ \,\-\_]*)$")
            if (pattern_text.match(request.form['nom_met']) and pattern_text.match(request.form['code_met'])
                    and pattern_text.match(request.form['design_met'])):
                new_met = Metier.query.filter_by(code_met=request.form['code_met']).first()
                if(new_met):
                    new_met.nom_met=request.form['nom_met']
                    new_met.designation_met=request.form['design_met']
                else:
                    new_met = Metier( request.form['code_met'] ,request.form['nom_met'] ,request.form['design_met'])
                
                new_met.commit()
                db.session.commit()
                return jsonify({"code": "ok", "message": "success"})
            else:
                return jsonify({"code": "fail", "message": "Veuillez respecter les combinaisons des champs."})

@admin.route("/add_fam", methods=['GET','POST'])
@login_required
@requires_roles('admin')
def add_fam():
    if request.method == 'GET':
        mets = Metier.query.all()
        data2 = []
        for m in mets:
            data2.append(m.todict())
        fams = Famille.query.all()
        data = []
        for m in fams:
            data.append(m.todict())
        return render_template('add_fam.html',fams=data,mets=data2)
    else:
        if request.method == 'POST':
            pattern_text = re.compile("^([0-9A-Za-zéèàç'\ \,\-\_]*)$")
            if (pattern_text.match(request.form['nom_fam']) and pattern_text.match(request.form['code_fam']) and pattern_text.match(request.form['metier'])
                    and pattern_text.match(request.form['design_fam'])):
                new_fam = Famille.query.filter_by(code_met=request.form['metier'],code_fam=request.form['code_fam']).first()
                if(new_fam):
                    new_fam.nom_fam=request.form['nom_fam']
                    new_fam.designation_fam=request.form['design_fam']
                else:
                    new_fam = Famille( request.form['code_fam'] ,request.form['nom_fam'] ,request.form['design_fam'],request.form['metier'])
                
                new_fam.commit()
                db.session.commit()
                return jsonify({"code": "ok", "message": "success"})
            else:
                return jsonify({"code": "fail", "message": "Veuillez respecter les combinaisons des champs."})

@admin.route("/delete_mets", methods=['POST'])
@login_required
@requires_roles('admin')
def delete_mets():
    if request.method == 'POST':
        pattern_text = re.compile("^([0-9A-Za-z\ \,\-\_]*)$")
        if (pattern_text.match(request.form['str'])):
            resource = request.form['str']
            app = Metier.query.filter_by(code_met=resource).first()
            if(app):
                listf = Famille.query.filter_by(code_met=resource).all()
                for lf in listf:
                    listsf = SousFamille.query.filter_by(code_fam=lf.code_fam).all()
                    for lsf in listsf:
                        Article.query.filter_by(code_sfam=lsf.code_sfam).delete()
                        db.session.commit()

                    SousFamille.query.filter_by(code_fam=lf.code_fam).delete()
                    db.session.commit()
                Famille.query.filter_by(code_met=resource).delete()
                db.session.commit()
                Metier.query.filter_by(code_met=resource).delete()
                # new_log = Logs('a supprimer le métier n° = ' + app.code_met.__str__() + ' ;', current_user)
                # new_log.commit()
                db.session.commit()
                # todo: ne fonctionne pas
            if app:
                return jsonify({"code": "ok", "message": "success"})
            else:
                return jsonify({"code": "fail",
                "message": "Vous ne pouvez pas supprimer ce métier !!"})
        else:
            return jsonify({"code": "fail",
                "message": "Veuillez respecter les combinaisons des champs. !!"})

@admin.route("/delete_fams", methods=['POST'])
@login_required
@requires_roles('admin')
def delete_fams():
    if request.method == 'POST':
        pattern_text = re.compile("^([0-9A-Za-z\ \,\-\_]*)$")
        if (pattern_text.match(request.form['str']) and pattern_text.match(request.form['str2'])):
            resource = request.form['str']
            resourceMet = request.form['str2']
            app = Famille.query.filter_by(code_fam=resource,code_met=resourceMet).first()
            if(app):
                listf = SousFamille.query.filter_by(code_fam=resource,code_met=resourceMet).all()
                for lf in listf:
                    Article.query.filter_by(code_sfam=lf.code_sfam,code_fam=resource,code_met=resourceMet).delete()
                    db.session.commit()
                    SousFamille.query.filter_by(code_sfam=lf.code_sfam,code_fam=resource,code_met=resourceMet).delete()
                    db.session.commit()

                Famille.query.filter_by(code_fam=resource,code_met=resourceMet).delete()
                db.session.commit()
                # new_log = Logs('a supprimer le métier n° = ' + app.code_met.__str__() + ' ;', current_user)
                # new_log.commit()
                
            if app:
                return jsonify({"code": "ok", "message": "success"})
            else:
                return jsonify({"code": "fail",
                "message": "Vous ne pouvez pas supprimer ce métier !!"})
        else:
            return jsonify({"code": "fail",
                "message": "Veuillez respecter les combinaisons des champs. !!"})


@admin.route("/add_sfam", methods=['GET','POST'])
@login_required
@requires_roles('admin')
def add_sfam():
    if request.method == 'GET':
        mets = Metier.query.all()
        data2 = []
        for m in mets:
            data2.append(m.todict())
        sfams = SousFamille.query.all()
        data = []
        for m in sfams:
            data.append(m.todict())
        return render_template('add_sfam.html',sfams=data,mets=data2)
    else:
        if request.method == 'POST':
            pattern_text = re.compile("^([0-9A-Za-z\ \,\-\_]*)$")
            if (pattern_text.match(request.form['nom_sfam']) and pattern_text.match(request.form['code_sfam']) and pattern_text.match(request.form['metier'])
                     and pattern_text.match(request.form['famille']) and pattern_text.match(request.form['design_sfam'])):
                new_sfam = SousFamille.query.filter_by(code_met=request.form['metier'],code_fam=request.form['famille'],code_sfam=request.form['code_sfam']).first()
                if(new_sfam):
                    new_sfam.nom_sfam=request.form['nom_sfam']
                    new_sfam.designation_sfam=request.form['design_sfam']
                else:
                    new_sfam = SousFamille( request.form['code_sfam'] ,request.form['nom_sfam'] ,request.form['design_sfam'],request.form['metier'],request.form['famille'])
                
                new_sfam.commit()
                db.session.commit()
                return jsonify({"code": "ok", "message": "success"})
            else:
                return jsonify({"code": "fail", "message": "Veuillez respecter les combinaisons des champs."})


@admin.route("/delete_sfams", methods=['POST'])
@login_required
@requires_roles('admin')
def delete_sfams():
    if request.method == 'POST':
        pattern_text = re.compile("^([0-9A-Za-z\ \,\-\_]*)$")
        if (pattern_text.match(request.form['str']) and pattern_text.match(request.form['str2']) and pattern_text.match(request.form['str3'])):
            resource = request.form['str']
            resourceMet = request.form['str2']
            resourceFam = request.form['str3']
            app = SousFamille.query.filter_by(code_sfam=resource,code_fam=resourceFam,code_met=resourceMet).first()
            if(app):
                Article.query.filter_by(code_sfam=resource,code_fam=resourceFam,code_met=resourceMet).delete()
                db.session.commit()
                SousFamille.query.filter_by(code_sfam=resource,code_fam=resourceFam,code_met=resourceMet).delete()
                db.session.commit()
                # new_log = Logs('a supprimer le métier n° = ' + app.code_met.__str__() + ' ;', current_user)
                # new_log.commit()
                
            if app:
                return jsonify({"code": "ok", "message": "success"})
            else:
                return jsonify({"code": "fail",
                "message": "Vous ne pouvez pas supprimer ce métier !!"})
        else:
            return jsonify({"code": "fail",
                "message": "Veuillez respecter les combinaisons des champs. !!"})

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in Config.ALLOWED_EXTENSIONS


@admin.route("/delete_arts", methods=['POST'])
@login_required
@requires_roles('admin')
def delete_arts():
    if request.method == 'POST':
        pattern_text = re.compile("^([0-9A-Za-z\ \,\-\_]*)$")
        if (pattern_text.match(request.form['str']) and pattern_text.match(request.form['str2']) 
                                and pattern_text.match(request.form['str3'])  and pattern_text.match(request.form['str4'])):
            resource = request.form['str']
            resourceMet = request.form['str2']
            resourceFam = request.form['str3']
            resourceSFAM = request.form['str4']
            app = Article.query.filter_by(code_art=resource,code_sfam=resourceSFAM,code_fam=resourceFam,code_met=resourceMet).first()
            if(app):
                Article.query.filter_by(code_art=resource,code_sfam=resourceSFAM,code_fam=resourceFam,code_met=resourceMet).delete()
                db.session.commit()
                # new_log = Logs('a supprimer le métier n° = ' + app.code_met.__str__() + ' ;', current_user)
                # new_log.commit()
                
            if app:
                return jsonify({"code": "ok", "message": "success"})
            else:
                return jsonify({"code": "fail",
                "message": "Vous ne pouvez pas supprimer cet Article !!"})
        else:
            return jsonify({"code": "fail",
                "message": "Veuillez respecter les combinaisons des champs. !!"})

@admin.route('/print_articles', methods=['GET'])
@login_required
@requires_roles('admin')
def getpdf():
    global no_valids_app
    
    try:
        if current_user.is_authenticated:
            rdv = Article.query.all()
            data = []
            for r in rdv:
                data.append(r.todict_min())
            if (rdv):
                page = render_template('print_arts.html', rdv=data, now=datetime.today().strftime('%d/%m/%Y'))
                options = {
                    'quiet': '',
                    'encoding': "UTF-8",
                    "enable-local-file-access": None
                }
                file_name=current_user.pseudo_cpt +'.'+ datetime.today().strftime('%d.%m.%Y')
                conf = pdfkit.configuration(wkhtmltopdf=Config.WKHTMLTOPDF_BIN_PATH)
                pdf = pdfkit.from_string(page,False,
                #  os.path.dirname(os.path.realpath(__file__))+'/pdfs/'+file_name +'.pdf',
                css= os.path.dirname(os.path.realpath(__file__))+'/../static/plugins/bootstrap/css/bootstrap.min.css' , options=options,
                configuration=conf)
                response = make_response(pdf)
                response.headers['Content-Type'] = 'application/pdf'
                response.headers['Content-Disposition'] = 'inline; filename='+file_name+'.pdf'
                
                return response
                # return send_from_directory(directory=os.path.dirname(os.path.realpath(__file__))+'/pdfs', filename= file_name+'.pdf', as_attachment=True)
                # os.remove(os.path.dirname(os.path.realpath(__file__))+'/pdfs/'+file_name+'.pdf')
            else:
                return redirect("/admin/list_article")
    except TemplateNotFound:
        abort(404)

@admin.route('/export_excel.csv', methods=['GET'])
@login_required
@requires_roles('admin')
def getexcel():
    global no_valids_app
    
    try:
        if current_user.is_authenticated:
            rdv = Article.query.all()
            data = [['code','métier','famille','sous famille','nom']]
            for r in rdv:
                data.append(r.todict_excel())
            if (rdv):
                return excel.make_response_from_array(data,"csv")
            else:
                return redirect("/admin/list_article")
    except TemplateNotFound:
        abort(404)