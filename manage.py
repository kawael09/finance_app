# coding=utf-8
import os
from datetime import timedelta
from flask import Flask, render_template, redirect, request, jsonify,session
from flask_login import LoginManager, login_user, current_user, login_required, logout_user
from functools import wraps
from flask_bcrypt import Bcrypt
from models import db as route_db,User,Logs
from validators import CompteSchema
from conf.config import Config
import logging, click
from conf.config import Config
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_static_compress import FlaskStaticCompress
from flask_sessionstore import Session
from flask_session_captcha import FlaskSessionCaptcha
from marshmallow import ValidationError
from flask_compress import Compress
import flask_excel as excel
from waitress import serve

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = Config.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_BINDS'] = Config.SQLALCHEMY_BINDS
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = Config.SQLALCHEMY_TRACK_MODIFICATIONS
app.config['SECRET_KEY'] = Config.SECRET_KEY
app.config['CAPTCHA_ENABLE'] = True
app.config['CAPTCHA_LENGTH'] = 5
app.config['CAPTCHA_WIDTH'] = 160
app.config['CAPTCHA_HEIGHT'] = 60
app.config['SESSION_TYPE'] = 'sqlalchemy'
app.config['SECURITY_TRACKABLE'] = True
app.config['DEBUG'] = Config.DEBUG
app.config['UPLOAD_FOLDER'] = Config.UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = Config.MAX_CONTENT_LENGTH  # 1Go
Session(app)
Compress(app)
excel.init_excel(app)
captcha = FlaskSessionCaptcha(app)
login_manager = LoginManager()
bcrypt = Bcrypt(app)
compress = FlaskStaticCompress(app)
limiter = Limiter(
    app,
    key_func=get_remote_address,
    # default_limits=["2/second"]
)


def requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if get_current_user_role() not in roles:
                return unauthorized_callback()
            return f(*args, **kwargs)

        return wrapped

    return wrapper


def get_current_user_role():
    return current_user.type_usr


@login_manager.user_loader
def load_user(id_usr):
    return User.query.get(int(id_usr))


@app.route("/", methods=['GET', 'POST'])
def home():
    return redirect("/login")

@app.route("/login", methods=['GET', 'POST'])
# @limiter.limit("2/seconde")
def login():
    try:
        if request.method == 'GET':
            return render_template('login.html')
        else:
            if captcha.validate(value=request.form['captcha']):
                # with app.app_context():
                    # try:
                # print(bcrypt.generate_password_hash(request.form['password']))
            
                compte = User.query.filter_by(username_usr=request.form['username']).first()
                if (compte is not None and bcrypt.check_password_hash(compte.pass_usr, request.form['password'])
                        and compte.etat_usr == 'active'):
                    login_user(compte)
                        # add logs
                        # app.logger.info('%s loged in successfully', current_user.id_usr)
                    new_log = Logs('s\'est connecté;', compte)
                    new_log.commit()
                    return jsonify({"message": "success", "root": compte.type_usr})
                else:
                    return jsonify({"message": "Authentification échouée !! ","captcha": captcha.generate()})
            else:
                return jsonify({"message": "Captcha non Valide !!", "captcha": captcha.generate()})
                # except ValidationError as err:
                #     return jsonify({"message": "Veuillez respecter les formats des insertions !! "})
    except Exception as ex:
        print(ex)
        return render_template('login.html')

@app.route("/regene_captcha", methods=['POST'])
def genere_captcha():
    return jsonify({"captcha": captcha.generate()})


# @app.route("/register", methods=['GET', 'POST'])
# @limiter.limit("2/seconde")
# def register():
#     try:
#         if request.method == 'GET':
#             return render_template('register.html')
#         else:
#             if captcha.validate(value=request.form['captcha']):
#                 with app.app_context():
#                     compte = Compte.query.filter_by(pseudo_cpt=request.form['username']).first()
#                     if compte is None:
#                         stru = Structure.query.filter_by(nom_str=request.form['structure'],region_str=request.form['region']).first()
#                         if stru is None:
#                             stru = Structure(request.form['structure'],request.form['region'])
#                             stru.commit()
#                         compte = Compte(request.form['username'],
#                                         bcrypt.generate_password_hash(request.form['password']),
#                                         "en attente", "user", stru.id_str)
#                         compte.commit()
#                         new_log = Logs('s\'est inscrit;', compte)
#                         new_log.commit()
#                         app.logger.info('%s registred successfully')
#                         return jsonify({"message": "success", "root": compte.type_cpt})
#                     else:
#                         return jsonify({"message": "Ce compte existe déja !! ", "captcha": captcha.generate()})
#             else:
#                 return jsonify({"message": "Captcha non Valide !!", "captcha": captcha.generate()})
#     except Exception as ex:
#         print(ex)
#         return jsonify({"message": ex.__str__(), "captcha": captcha.generate()})

@app.route("/logout")
@login_required
def logout():
    app.logger.info('%s logged out successfully', current_user.id_usr)
    logout_user()
    # new_log = Logs('s\'est déconnecté;', current_user)
    # new_log.commit()
    return redirect("/login")


@login_manager.unauthorized_handler
def unauthorized_callback():
    return redirect('/login')


@app.errorhandler(429)
def ratelimit_handler(e):
    return render_template('429.html'), 429


@app.errorhandler(400)
def page_not_found(error):
    # dir_path = os.path.dirname(os.path.realpath(__file__))
    # with open(dir_path + '/admin/sections.json') as json_data_file:
    #     data = json.load(json_data_file)
    # print(error)
    return render_template('400.html')
@app.errorhandler(404)
def page_not_found2(error):
    # dir_path = os.path.dirname(os.path.realpath(__file__))
    # with open(dir_path + '/admin/sections.json') as json_data_file:
    #     data = json.load(json_data_file)
    # print(error)
    return render_template('404.html')


if __name__ == "__main__":
    
    app.permanent_session_lifetime = timedelta(minutes=5)
    logger = logging.getLogger('waitress')
    logger.setLevel(logging.WARNING)
    # app.wsgi_app = StreamConsumingMiddleware(app.wsgi_app)
    # try:
    # from admin.routes import admin
    from comptable.routes import comptable

    # app.register_blueprint(admin, url_prefix="/admin")
    app.register_blueprint(comptable, url_prefix="/comptable")
    with app.app_context():
        route_db.init_app(app)
        # route_ma.init_app(app)
        login_manager.init_app(app)

    # app.run('localhost', debug=True,port=80)
    # todo: run on production .
    serve(app, host='0.0.0.0',port=80)
