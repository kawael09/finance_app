CREATE DATABASE  IF NOT EXISTS `finance_prevision` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `finance_prevision`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: finance_prevision
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_prev`
--

DROP TABLE IF EXISTS `article_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_prev` (
  `ID_ART` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_ART` varchar(100) DEFAULT NULL,
  `SOLDE_INIT_ART` double(45,5) DEFAULT NULL,
  `SOLDE_ACTUEL_ART` double(45,5) DEFAULT NULL,
  `ID_CHP` int(11) NOT NULL,
  PRIMARY KEY (`ID_ART`),
  KEY `fk_article_chapitre1_idx` (`ID_CHP`),
  CONSTRAINT `fk_article_chapitre1` FOREIGN KEY (`ID_CHP`) REFERENCES `chapitre_prev` (`ID_CHP`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_prev`
--

LOCK TABLES `article_prev` WRITE;
/*!40000 ALTER TABLE `article_prev` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budget_prev`
--

DROP TABLE IF EXISTS `budget_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget_prev` (
  `ID_BDG` int(11) NOT NULL AUTO_INCREMENT,
  `ANNEE_BDG` int(11) DEFAULT NULL,
  `NOM_BDG` varchar(100) DEFAULT NULL,
  `TYPE_BDG` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_BDG` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_BDG` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_BDG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget_prev`
--

LOCK TABLES `budget_prev` WRITE;
/*!40000 ALTER TABLE `budget_prev` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chapitre_prev`
--

DROP TABLE IF EXISTS `chapitre_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapitre_prev` (
  `ID_CHP` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_CHP` varchar(100) DEFAULT NULL,
  `SOLDE_INIT_CHP` double(45,5) DEFAULT NULL,
  `SOLDE_ACTUEL_CHP` double(45,5) DEFAULT NULL,
  `ID_BDG` int(11) NOT NULL,
  PRIMARY KEY (`ID_CHP`),
  KEY `fk_chapitre_budget1_idx` (`ID_BDG`),
  CONSTRAINT `fk_chapitre_budget1` FOREIGN KEY (`ID_BDG`) REFERENCES `budget_prev` (`ID_BDG`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapitre_prev`
--

LOCK TABLES `chapitre_prev` WRITE;
/*!40000 ALTER TABLE `chapitre_prev` DISABLE KEYS */;
/*!40000 ALTER TABLE `chapitre_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prevision`
--

DROP TABLE IF EXISTS `prevision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prevision` (
  `ID_PREV` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_PREV` varchar(100) DEFAULT NULL,
  `DATE_PREV` datetime DEFAULT NULL,
  `SOLDE_PREV` double(45,2) DEFAULT NULL,
  `ID_ART` int(11) NOT NULL,
  `ID_USR` int(11) NOT NULL,
  PRIMARY KEY (`ID_PREV`),
  KEY `fk_prevision_article_idx` (`ID_ART`),
  KEY `fk_prevision_user1_idx` (`ID_USR`),
  CONSTRAINT `fk_prevision_article` FOREIGN KEY (`ID_ART`) REFERENCES `article_prev` (`ID_ART`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prevision_user1` FOREIGN KEY (`ID_USR`) REFERENCES `finance_camssp`.`user` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prevision`
--

LOCK TABLES `prevision` WRITE;
/*!40000 ALTER TABLE `prevision` DISABLE KEYS */;
/*!40000 ALTER TABLE `prevision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) DEFAULT NULL,
  `data` text,
  `expiry` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,'session:9a83dc82-02ff-4642-b8bc-f0702c122e0d','{\"_fresh\":false,\"_permanent\":true}','2020-10-13 14:57:34'),(2,'session:6a5c6680-5512-4b18-9f50-1ef7e1134037','{\"_fresh\":false,\"_permanent\":true}','2020-11-08 08:32:40'),(3,'session:17e967c4-f4fb-4858-881a-84764e96ca7f','{\"_fresh\":false,\"_permanent\":true}','2020-11-24 08:36:14'),(4,'session:2eee58f5-0253-4828-833b-9236aedbed0b','{\"_fresh\":false,\"_permanent\":true}','2020-11-24 09:17:18'),(5,'session:5f579ae8-4d4c-44af-a417-7b76197611ca','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":1}','2020-11-24 09:58:47'),(6,'session:e161819f-38e2-48dd-b060-559f40b6a670','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":1}','2020-11-24 10:15:50'),(7,'session:91a90463-4d75-4de5-9655-0b0156431a83','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":1}','2020-11-24 10:15:51'),(8,'session:dbea0778-a138-4b0f-9e97-692b8abc893c','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":1}','2020-11-24 10:15:51'),(9,'session:35129f9d-0397-4a0a-a0ea-6752ddf454bd','{\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true}','2020-11-24 10:17:49'),(10,'session:a537255b-931a-4781-81c8-4ce736f4626f','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":1}','2020-11-24 10:46:27'),(11,'session:3a4f2cc1-a947-48e6-9361-f4920417f8e3','{\"_fresh\":false,\"_permanent\":true}','2020-11-24 10:52:31'),(12,'session:4b61f1ff-fab5-46d1-80b9-7e78fb51f903','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":2}','2020-11-24 13:00:09'),(13,'session:f95a30a8-8a2c-46b1-a3a1-f1c38a651190','{\"_fresh\":false,\"_permanent\":true}','2020-11-24 13:36:11'),(14,'session:6bba69c4-8583-44cc-b974-9e12047f5bb7','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":2}','2020-11-24 14:23:12'),(15,'session:f3cca6b7-302a-4aac-97cc-51b17da6881c','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":2}','2020-11-25 12:26:39'),(16,'session:020341c2-7664-4faa-a557-0974f4cc4677','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":2}','2020-11-25 13:15:52'),(17,'session:547a5c48-bb28-4821-8643-991cd6e63832','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":2}','2020-11-25 13:29:22'),(18,'session:0314d45a-dd80-4a16-838b-46a220829cde','{\"_fresh\":true,\"_id\":\"22d9f98c358cb5b1320b9d5f8249c030f96eebab509bdc20228d83aa183fe8cc771b39478763151911898c00f9b45c113da0f27921368988d31fea61d6cdecbd\",\"_permanent\":true,\"user_id\":2}','2020-11-25 13:38:59');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-26 15:03:14
