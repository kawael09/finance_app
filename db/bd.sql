CREATE DATABASE  IF NOT EXISTS `finance_prevision` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `finance_prevision`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: finance_prevision
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_prev`
--

DROP TABLE IF EXISTS `article_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_prev` (
  `ID_ART` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_ART` varchar(100) DEFAULT NULL,
  `SOLDE_INIT_ART` double(45,5) DEFAULT NULL,
  `SOLDE_ACTUEL_ART` double(45,5) DEFAULT NULL,
  `ID_CHP` int(11) NOT NULL,
  PRIMARY KEY (`ID_ART`),
  KEY `fk_article_chapitre1_idx` (`ID_CHP`),
  CONSTRAINT `fk_article_chapitre1` FOREIGN KEY (`ID_CHP`) REFERENCES `chapitre_prev` (`ID_CHP`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_prev`
--

LOCK TABLES `article_prev` WRITE;
/*!40000 ALTER TABLE `article_prev` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budget_prev`
--

DROP TABLE IF EXISTS `budget_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget_prev` (
  `ID_BDG` int(11) NOT NULL AUTO_INCREMENT,
  `ANNEE_BDG` int(11) DEFAULT NULL,
  `NOM_BDG` varchar(100) DEFAULT NULL,
  `TYPE_BDG` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_BDG` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_BDG` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_BDG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget_prev`
--

LOCK TABLES `budget_prev` WRITE;
/*!40000 ALTER TABLE `budget_prev` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chapitre_prev`
--

DROP TABLE IF EXISTS `chapitre_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapitre_prev` (
  `ID_CHP` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_CHP` varchar(100) DEFAULT NULL,
  `SOLDE_INIT_CHP` double(45,5) DEFAULT NULL,
  `SOLDE_ACTUEL_CHP` double(45,5) DEFAULT NULL,
  `ID_BDG` int(11) NOT NULL,
  PRIMARY KEY (`ID_CHP`),
  KEY `fk_chapitre_budget1_idx` (`ID_BDG`),
  CONSTRAINT `fk_chapitre_budget1` FOREIGN KEY (`ID_BDG`) REFERENCES `budget_prev` (`ID_BDG`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapitre_prev`
--

LOCK TABLES `chapitre_prev` WRITE;
/*!40000 ALTER TABLE `chapitre_prev` DISABLE KEYS */;
/*!40000 ALTER TABLE `chapitre_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prevision`
--

DROP TABLE IF EXISTS `prevision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prevision` (
  `ID_PREV` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_PREV` varchar(100) DEFAULT NULL,
  `DATE_PREV` datetime DEFAULT NULL,
  `SOLDE_PREV` double(45,2) DEFAULT NULL,
  `ID_ART` int(11) NOT NULL,
  `ID_USR` int(11) NOT NULL,
  PRIMARY KEY (`ID_PREV`),
  KEY `fk_prevision_article_idx` (`ID_ART`),
  KEY `fk_prevision_user1_idx` (`ID_USR`),
  CONSTRAINT `fk_prevision_article` FOREIGN KEY (`ID_ART`) REFERENCES `article_prev` (`ID_ART`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prevision_user1` FOREIGN KEY (`ID_USR`) REFERENCES `finance_camssp`.`user` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prevision`
--

LOCK TABLES `prevision` WRITE;
/*!40000 ALTER TABLE `prevision` DISABLE KEYS */;
/*!40000 ALTER TABLE `prevision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) DEFAULT NULL,
  `data` text,
  `expiry` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,'session:9a83dc82-02ff-4642-b8bc-f0702c122e0d','{\"_fresh\":false,\"_permanent\":true}','2020-10-13 14:57:34');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-13 16:05:13
CREATE DATABASE  IF NOT EXISTS `finance_camssp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `finance_camssp`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: finance_camssp
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alimenter`
--

DROP TABLE IF EXISTS `alimenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alimenter` (
  `ID_CPTP` int(11) DEFAULT NULL,
  `ID_CPTS` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `DATE_ALIM` datetime DEFAULT NULL,
  `SOMME_ALIM` double(20,2) DEFAULT NULL,
  KEY `FK_ALIMENTE_CPT_P` (`ID_CPTP`),
  KEY `FK_ALIMENTE_CPT_S` (`ID_CPTS`),
  KEY `FK_ALIMANTE_USER` (`ID_USR`),
  CONSTRAINT `FK_ALIMANTE_USER` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`),
  CONSTRAINT `FK_ALIMENTE_CPT_P` FOREIGN KEY (`ID_CPTP`) REFERENCES `compte_p` (`ID_CPTP`),
  CONSTRAINT `FK_ALIMENTE_CPT_S` FOREIGN KEY (`ID_CPTS`) REFERENCES `compte_s` (`ID_CPTS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alimenter`
--

LOCK TABLES `alimenter` WRITE;
/*!40000 ALTER TABLE `alimenter` DISABLE KEYS */;
/*!40000 ALTER TABLE `alimenter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alimenter_chp`
--

DROP TABLE IF EXISTS `alimenter_chp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alimenter_chp` (
  `ID_CPTP` int(11) NOT NULL DEFAULT '0',
  `ID_CPTS` int(11) NOT NULL DEFAULT '0',
  `ID_BDG` int(11) NOT NULL DEFAULT '0',
  `ID_USR` int(11) NOT NULL DEFAULT '0',
  `DATE_ALIM` datetime DEFAULT NULL,
  `SOMME_ALIM` double(20,2) DEFAULT NULL,
  PRIMARY KEY (`ID_CPTS`,`ID_BDG`,`ID_USR`,`ID_CPTP`),
  KEY `FK_ALIM_BDG` (`ID_BDG`),
  KEY `FK_ALIM_CPT_S` (`ID_CPTS`),
  KEY `FK_ALIM_USR` (`ID_USR`),
  CONSTRAINT `FK_ALIM_BDG` FOREIGN KEY (`ID_BDG`) REFERENCES `budget` (`ID_BDG`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ALIM_CPT_S` FOREIGN KEY (`ID_CPTS`) REFERENCES `compte_s` (`ID_CPTS`),
  CONSTRAINT `FK_ALIM_USR` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alimenter_chp`
--

LOCK TABLES `alimenter_chp` WRITE;
/*!40000 ALTER TABLE `alimenter_chp` DISABLE KEYS */;
/*!40000 ALTER TABLE `alimenter_chp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `ID_ART` int(11) NOT NULL,
  `ID_CHP` int(11) DEFAULT NULL,
  `NOM_ART` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_ART` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_ART` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_ART`),
  KEY `FK_ART_CHP` (`ID_CHP`),
  CONSTRAINT `FK_ART_CHP` FOREIGN KEY (`ID_CHP`) REFERENCES `chapitre` (`ID_CHP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budget`
--

DROP TABLE IF EXISTS `budget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget` (
  `ID_BDG` int(11) NOT NULL,
  `NOM_BDG` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_BDG` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_BDG` double(45,2) DEFAULT NULL,
  `TYPE_BDG` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID_BDG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget`
--

LOCK TABLES `budget` WRITE;
/*!40000 ALTER TABLE `budget` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chapitre`
--

DROP TABLE IF EXISTS `chapitre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapitre` (
  `ID_CHP` int(11) NOT NULL,
  `NOM_CHP` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_CHP` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_CHP` double(45,2) DEFAULT NULL,
  `ID_BDG` int(11) NOT NULL,
  PRIMARY KEY (`ID_CHP`,`ID_BDG`),
  KEY `fk_chapitre_budget1_idx` (`ID_BDG`),
  CONSTRAINT `fk_chapitre_budget1` FOREIGN KEY (`ID_BDG`) REFERENCES `budget` (`ID_BDG`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapitre`
--

LOCK TABLES `chapitre` WRITE;
/*!40000 ALTER TABLE `chapitre` DISABLE KEYS */;
/*!40000 ALTER TABLE `chapitre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compte_p`
--

DROP TABLE IF EXISTS `compte_p`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compte_p` (
  `ID_CPTP` int(11) NOT NULL,
  `NUMERO_CPTP` varchar(45) DEFAULT NULL,
  `NOM_CPTP` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_CPTP` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_CPTP` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_CPTP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compte_p`
--

LOCK TABLES `compte_p` WRITE;
/*!40000 ALTER TABLE `compte_p` DISABLE KEYS */;
/*!40000 ALTER TABLE `compte_p` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compte_s`
--

DROP TABLE IF EXISTS `compte_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compte_s` (
  `ID_CPTS` int(11) NOT NULL,
  `NUMERO_CPTS` varchar(45) DEFAULT NULL,
  `NOM_CPTS` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_CPTS` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_CPTS` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_CPTS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compte_s`
--

LOCK TABLES `compte_s` WRITE;
/*!40000 ALTER TABLE `compte_s` DISABLE KEYS */;
/*!40000 ALTER TABLE `compte_s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `depense`
--

DROP TABLE IF EXISTS `depense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `depense` (
  `ID_DPS` int(11) NOT NULL,
  `ID_ART` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `NOM_DPS` varchar(45) DEFAULT NULL,
  `DATE_DPS` datetime DEFAULT NULL,
  `SOLDE_DPS` double(50,2) DEFAULT NULL,
  PRIMARY KEY (`ID_DPS`),
  KEY `FK_DEP_ART` (`ID_ART`),
  KEY `FK_REFERENCE_14` (`ID_USR`),
  CONSTRAINT `FK_DEP_ART` FOREIGN KEY (`ID_ART`) REFERENCES `article` (`ID_ART`),
  CONSTRAINT `FK_REFERENCE_14` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `depense`
--

LOCK TABLES `depense` WRITE;
/*!40000 ALTER TABLE `depense` DISABLE KEYS */;
/*!40000 ALTER TABLE `depense` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `ID_LOG` int(11) NOT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `ACTION_LOG` varchar(100) DEFAULT NULL,
  `DATE_LOG` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_LOG`),
  KEY `FK_REFERENCE_13` (`ID_USR`),
  CONSTRAINT `FK_REFERENCE_13` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `ID_RGN` int(11) NOT NULL,
  `NOM_RGN` varchar(45) DEFAULT NULL,
  `CODE_RGN` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID_RGN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revenue`
--

DROP TABLE IF EXISTS `revenue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revenue` (
  `ID_REV` int(11) NOT NULL,
  `ID_CPTP` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `NOM_REV` varchar(45) DEFAULT NULL,
  `TYPE_REV` varchar(45) DEFAULT NULL,
  `SOLDE_REV` double(20,2) DEFAULT NULL,
  PRIMARY KEY (`ID_REV`),
  KEY `FK_REV_CPT_P` (`ID_CPTP`),
  KEY `FK_REV_USR` (`ID_USR`),
  CONSTRAINT `FK_REV_CPT_P` FOREIGN KEY (`ID_CPTP`) REFERENCES `compte_p` (`ID_CPTP`),
  CONSTRAINT `FK_REV_USR` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revenue`
--

LOCK TABLES `revenue` WRITE;
/*!40000 ALTER TABLE `revenue` DISABLE KEYS */;
/*!40000 ALTER TABLE `revenue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID_USR` int(11) NOT NULL AUTO_INCREMENT,
  `ID_RGN` int(11) DEFAULT NULL,
  `USERNAME_USR` datetime DEFAULT NULL,
  `PASS_USR` datetime DEFAULT NULL,
  `ETAT_USR` datetime DEFAULT NULL,
  `TYPE_USR` datetime DEFAULT NULL,
  `DATE_CREATE_USR` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_USR`),
  KEY `FK_REGION_CPT` (`ID_RGN`),
  CONSTRAINT `FK_REGION_CPT` FOREIGN KEY (`ID_RGN`) REFERENCES `region` (`ID_RGN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-13 16:05:13
