CREATE DATABASE  IF NOT EXISTS `finance_prevision` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `finance_prevision`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: finance_prevision
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article_prev`
--

DROP TABLE IF EXISTS `article_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_prev` (
  `ID_ART` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_ART` varchar(100) DEFAULT NULL,
  `SOLDE_INIT_ART` double(45,5) DEFAULT NULL,
  `SOLDE_ACTUEL_ART` double(45,5) DEFAULT NULL,
  `ID_CHP` int(11) NOT NULL,
  `ID_USR` int(11) NOT NULL,
  `DATE_ART` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_ART`),
  KEY `fk_article_chapitre1_idx` (`ID_CHP`),
  KEY `fk_article_user22_idx` (`ID_USR`),
  CONSTRAINT `fk_article_chapitre1` FOREIGN KEY (`ID_CHP`) REFERENCES `chapitre_prev` (`ID_CHP`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_article_user22` FOREIGN KEY (`ID_USR`) REFERENCES `finance_camssp`.`user` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_prev`
--

LOCK TABLES `article_prev` WRITE;
/*!40000 ALTER TABLE `article_prev` DISABLE KEYS */;
INSERT INTO `article_prev` VALUES (1,'hammamet',8790.00000,8000.00000,1,1,'2021-03-15 10:56:15'),(2,'bengalos',10000.00000,5000.00000,1,1,'2021-03-15 10:56:40');
/*!40000 ALTER TABLE `article_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budget_prev`
--

DROP TABLE IF EXISTS `budget_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget_prev` (
  `ID_BDG` int(11) NOT NULL AUTO_INCREMENT,
  `ANNEE_BDG` int(11) DEFAULT NULL,
  `NOM_BDG` varchar(100) DEFAULT NULL,
  `TYPE_BDG` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_BDG` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_BDG` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_BDG`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget_prev`
--

LOCK TABLES `budget_prev` WRITE;
/*!40000 ALTER TABLE `budget_prev` DISABLE KEYS */;
INSERT INTO `budget_prev` VALUES (1,2021,'budget annuel fonc','fonctionnel',12458790.00,12400000.00);
/*!40000 ALTER TABLE `budget_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chapitre_prev`
--

DROP TABLE IF EXISTS `chapitre_prev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapitre_prev` (
  `ID_CHP` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_CHP` varchar(100) DEFAULT NULL,
  `SOLDE_INIT_CHP` double(45,5) DEFAULT NULL,
  `SOLDE_ACTUEL_CHP` double(45,5) DEFAULT NULL,
  `ID_BDG` int(11) NOT NULL,
  `ID_USR` int(11) NOT NULL,
  `DATE_CHP` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_CHP`),
  KEY `fk_chapitre_budget1_idx` (`ID_BDG`),
  KEY `fk_chapitre_user3_idx` (`ID_USR`),
  CONSTRAINT `fk_chapitre_budget1` FOREIGN KEY (`ID_BDG`) REFERENCES `budget_prev` (`ID_BDG`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_chapitre_user3` FOREIGN KEY (`ID_USR`) REFERENCES `finance_camssp`.`user` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapitre_prev`
--

LOCK TABLES `chapitre_prev` WRITE;
/*!40000 ALTER TABLE `chapitre_prev` DISABLE KEYS */;
INSERT INTO `chapitre_prev` VALUES (1,'Loisirs',58790.00000,40000.00000,1,1,'2021-03-15 09:55:10');
/*!40000 ALTER TABLE `chapitre_prev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prevision`
--

DROP TABLE IF EXISTS `prevision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prevision` (
  `ID_PREV` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_PREV` varchar(100) DEFAULT NULL,
  `DATE_PREV` datetime DEFAULT NULL,
  `SOLDE_PREV` double(45,2) DEFAULT NULL,
  `ID_ART` int(11) NOT NULL,
  `ID_USR` int(11) NOT NULL,
  PRIMARY KEY (`ID_PREV`),
  KEY `fk_prevision_article_idx` (`ID_ART`),
  KEY `fk_prevision_user1_idx` (`ID_USR`),
  CONSTRAINT `fk_prevision_article` FOREIGN KEY (`ID_ART`) REFERENCES `article_prev` (`ID_ART`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_prevision_user1` FOREIGN KEY (`ID_USR`) REFERENCES `finance_camssp`.`user` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prevision`
--

LOCK TABLES `prevision` WRITE;
/*!40000 ALTER TABLE `prevision` DISABLE KEYS */;
INSERT INTO `prevision` VALUES (1,'crf tipaza','2021-03-15 11:32:44',5000.00,2,1),(2,'hammam melouane','2021-03-15 11:35:40',790.00,1,1);
/*!40000 ALTER TABLE `prevision` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-15 14:23:06
CREATE DATABASE  IF NOT EXISTS `finance_camssp` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `finance_camssp`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: finance_camssp
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alimenter`
--

DROP TABLE IF EXISTS `alimenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alimenter` (
  `ID_CPTP` int(11) DEFAULT NULL,
  `ID_CPTS` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `DATE_ALIM` datetime DEFAULT NULL,
  `SOMME_ALIM` double(20,2) DEFAULT NULL,
  KEY `FK_ALIMANTE_USER` (`ID_USR`),
  KEY `FK_ALIMENTE_CPT_P` (`ID_CPTP`),
  KEY `FK_ALIMENTE_CPT_S` (`ID_CPTS`),
  CONSTRAINT `FK_ALIMANTE_USER` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`),
  CONSTRAINT `FK_ALIMENTE_CPT_P` FOREIGN KEY (`ID_CPTP`) REFERENCES `compte_p` (`ID_CPTP`),
  CONSTRAINT `FK_ALIMENTE_CPT_S` FOREIGN KEY (`ID_CPTS`) REFERENCES `compte_s` (`ID_CPTS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alimenter`
--

LOCK TABLES `alimenter` WRITE;
/*!40000 ALTER TABLE `alimenter` DISABLE KEYS */;
/*!40000 ALTER TABLE `alimenter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alimenter_chp`
--

DROP TABLE IF EXISTS `alimenter_chp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alimenter_chp` (
  `ID_CPTP` int(11) NOT NULL DEFAULT '0',
  `ID_CPTS` int(11) NOT NULL DEFAULT '0',
  `ID_BDG` int(11) NOT NULL DEFAULT '0',
  `ID_USR` int(11) NOT NULL DEFAULT '0',
  `DATE_ALIM` datetime DEFAULT NULL,
  `SOMME_ALIM` double(20,2) DEFAULT NULL,
  PRIMARY KEY (`ID_CPTS`,`ID_BDG`,`ID_USR`,`ID_CPTP`),
  KEY `FK_ALIM_CPT_S` (`ID_CPTS`),
  KEY `FK_ALIM_USR` (`ID_USR`),
  KEY `FK_ALIM_BDG` (`ID_BDG`),
  CONSTRAINT `FK_ALIM_BDG` FOREIGN KEY (`ID_BDG`) REFERENCES `budget` (`ID_BDG`),
  CONSTRAINT `FK_ALIM_CPT_S` FOREIGN KEY (`ID_CPTS`) REFERENCES `compte_s` (`ID_CPTS`),
  CONSTRAINT `FK_ALIM_USR` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alimenter_chp`
--

LOCK TABLES `alimenter_chp` WRITE;
/*!40000 ALTER TABLE `alimenter_chp` DISABLE KEYS */;
/*!40000 ALTER TABLE `alimenter_chp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `ID_ART` int(11) NOT NULL,
  `ID_CHP` int(11) DEFAULT NULL,
  `NOM_ART` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_ART` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_ART` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_ART`),
  KEY `FK_ART_CHP` (`ID_CHP`),
  CONSTRAINT `FK_ART_CHP` FOREIGN KEY (`ID_CHP`) REFERENCES `chapitre` (`ID_CHP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `budget`
--

DROP TABLE IF EXISTS `budget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `budget` (
  `ID_BDG` int(11) NOT NULL,
  `NOM_BDG` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_BDG` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_BDG` double(45,2) DEFAULT NULL,
  `TYPE_BDG` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID_BDG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `budget`
--

LOCK TABLES `budget` WRITE;
/*!40000 ALTER TABLE `budget` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chapitre`
--

DROP TABLE IF EXISTS `chapitre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chapitre` (
  `ID_CHP` int(11) NOT NULL,
  `NOM_CHP` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_CHP` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_CHP` double(45,2) DEFAULT NULL,
  `ID_BDG` int(11) NOT NULL,
  PRIMARY KEY (`ID_CHP`,`ID_BDG`),
  KEY `fk_chapitre_budget1_idx` (`ID_BDG`),
  CONSTRAINT `fk_chapitre_budget1` FOREIGN KEY (`ID_BDG`) REFERENCES `budget` (`ID_BDG`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chapitre`
--

LOCK TABLES `chapitre` WRITE;
/*!40000 ALTER TABLE `chapitre` DISABLE KEYS */;
/*!40000 ALTER TABLE `chapitre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compte_p`
--

DROP TABLE IF EXISTS `compte_p`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compte_p` (
  `ID_CPTP` int(11) NOT NULL,
  `NUMERO_CPTP` varchar(45) DEFAULT NULL,
  `NOM_CPTP` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_CPTP` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_CPTP` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_CPTP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compte_p`
--

LOCK TABLES `compte_p` WRITE;
/*!40000 ALTER TABLE `compte_p` DISABLE KEYS */;
/*!40000 ALTER TABLE `compte_p` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compte_s`
--

DROP TABLE IF EXISTS `compte_s`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compte_s` (
  `ID_CPTS` int(11) NOT NULL,
  `NUMERO_CPTS` varchar(45) DEFAULT NULL,
  `NOM_CPTS` varchar(45) DEFAULT NULL,
  `SOLDE_INIT_CPTS` double(45,2) DEFAULT NULL,
  `SOLDE_ACTUEL_CPTS` double(45,2) DEFAULT NULL,
  PRIMARY KEY (`ID_CPTS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compte_s`
--

LOCK TABLES `compte_s` WRITE;
/*!40000 ALTER TABLE `compte_s` DISABLE KEYS */;
/*!40000 ALTER TABLE `compte_s` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `depense`
--

DROP TABLE IF EXISTS `depense`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `depense` (
  `ID_DPS` int(11) NOT NULL,
  `ID_ART` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `NOM_DPS` varchar(45) DEFAULT NULL,
  `DATE_DPS` datetime DEFAULT NULL,
  `SOLDE_DPS` double(50,2) DEFAULT NULL,
  PRIMARY KEY (`ID_DPS`),
  KEY `FK_DEP_ART` (`ID_ART`),
  KEY `FK_REFERENCE_14` (`ID_USR`),
  CONSTRAINT `FK_DEP_ART` FOREIGN KEY (`ID_ART`) REFERENCES `article` (`ID_ART`),
  CONSTRAINT `FK_REFERENCE_14` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `depense`
--

LOCK TABLES `depense` WRITE;
/*!40000 ALTER TABLE `depense` DISABLE KEYS */;
/*!40000 ALTER TABLE `depense` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `ID_LOG` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USR` int(11) DEFAULT NULL,
  `ACTION_LOG` varchar(100) DEFAULT NULL,
  `DATE_LOG` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_LOG`),
  KEY `FK_REFERENCE_13` (`ID_USR`),
  CONSTRAINT `FK_REFERENCE_13` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,1,'a créer l\'article n°= 1','2021-03-15 10:56:15'),(2,1,'a créer l\'article n°= 2','2021-03-15 10:56:40'),(3,1,'a créer la prévision n°= 1','2021-03-15 11:32:44'),(4,1,'a créer la prévision n°= 2','2021-03-15 11:35:40'),(5,1,'s\'est connecté;','2021-03-15 12:27:38'),(6,1,'s\'est connecté;','2021-03-15 13:13:12'),(7,1,'s\'est connecté;','2021-03-15 13:59:12');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `ID_RGN` int(11) NOT NULL,
  `NOM_RGN` varchar(45) DEFAULT NULL,
  `CODE_RGN` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID_RGN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (1,'BLIDA','09');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revenue`
--

DROP TABLE IF EXISTS `revenue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revenue` (
  `ID_REV` int(11) NOT NULL,
  `ID_CPTP` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `NOM_REV` varchar(45) DEFAULT NULL,
  `TYPE_REV` varchar(45) DEFAULT NULL,
  `SOLDE_REV` double(20,2) DEFAULT NULL,
  PRIMARY KEY (`ID_REV`),
  KEY `FK_REV_CPT_P` (`ID_CPTP`),
  KEY `FK_REV_USR` (`ID_USR`),
  CONSTRAINT `FK_REV_CPT_P` FOREIGN KEY (`ID_CPTP`) REFERENCES `compte_p` (`ID_CPTP`),
  CONSTRAINT `FK_REV_USR` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revenue`
--

LOCK TABLES `revenue` WRITE;
/*!40000 ALTER TABLE `revenue` DISABLE KEYS */;
/*!40000 ALTER TABLE `revenue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) DEFAULT NULL,
  `data` text,
  `expiry` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (1,'session:2341a235-cf66-49b2-bd83-78a8d6a9c986','{\"_fresh\":false,\"_permanent\":true}','2021-02-26 20:52:21'),(2,'session:542905fe-0f2d-4b9e-b779-4b6b99cef38e','{\"_fresh\":true,\"_id\":\"8aa521d26cd4ad8a7a379b3e5032e8c53e0ba8a9b874970296f868d25b5f3f5d7936881b1924f544f9d5b4550bb5bbb8dc3ad9bd36dd0233eb9505e688f382d8\",\"_permanent\":true,\"_user_id\":1}','2021-02-27 14:22:19'),(3,'session:c2ec78af-902a-417a-b633-8ca532d0ba4a','{\"_fresh\":true,\"_id\":\"ba3ded0390e9f9de70d64858e1f9bec2fb680ef760b7fd3eb3ed33b73255ad410e3aa2d79bb40a7c0a989c333b3725e4be81d9946283e18dc66642db01e7ca0b\",\"_permanent\":true,\"_user_id\":1}','2021-03-15 10:01:46'),(4,'session:98438b40-edb1-4678-8fef-ffd78c614c0e','{\"_fresh\":true,\"_id\":\"ba3ded0390e9f9de70d64858e1f9bec2fb680ef760b7fd3eb3ed33b73255ad410e3aa2d79bb40a7c0a989c333b3725e4be81d9946283e18dc66642db01e7ca0b\",\"_permanent\":true,\"_user_id\":1}','2021-03-15 10:40:50'),(5,'session:310d8d6a-c539-4881-a1f8-063f583cadf7','{\"_fresh\":true,\"_id\":\"ba3ded0390e9f9de70d64858e1f9bec2fb680ef760b7fd3eb3ed33b73255ad410e3aa2d79bb40a7c0a989c333b3725e4be81d9946283e18dc66642db01e7ca0b\",\"_permanent\":true,\"_user_id\":1}','2021-03-15 10:53:35'),(6,'session:918286ec-97cd-4397-a6a5-bfa4c441c991','{\"_permanent\":true}','2021-03-15 10:57:34'),(7,'session:4c49239b-357b-46df-8084-f2b68562ba97','{\"_fresh\":false,\"_permanent\":true,\"captcha_answer\":\"20365\"}','2021-03-15 11:33:29'),(8,'session:3937ab18-1f78-4157-9b0a-f8e02dd6a8a3','{\"_permanent\":true}','2021-03-15 11:10:28'),(9,'session:3d439639-c4a3-420a-bcdd-3cdfed4eef98','{\"_fresh\":false,\"_permanent\":true,\"captcha_answer\":\"84815\"}','2021-03-15 11:10:28'),(10,'session:5afb65bf-1db7-48ab-ab97-72a39be7fe9c','{\"_fresh\":false,\"_permanent\":true,\"captcha_answer\":\"68427\"}','2021-03-15 12:09:41'),(11,'session:6cb4b7c0-1762-4759-ad66-ca7e6873ae65','{\"_fresh\":true,\"_id\":\"ba3ded0390e9f9de70d64858e1f9bec2fb680ef760b7fd3eb3ed33b73255ad410e3aa2d79bb40a7c0a989c333b3725e4be81d9946283e18dc66642db01e7ca0b\",\"_permanent\":true,\"_user_id\":1,\"captcha_answer\":null}','2021-03-15 12:18:54'),(12,'session:e9f9e92b-e8fb-4e03-8720-177d5d0b100b','{\"_permanent\":true}','2021-03-15 12:19:15'),(13,'session:931f61ed-3e1d-42bd-8541-b294eeae6209','{\"_permanent\":true}','2021-03-15 12:19:16'),(14,'session:e480130f-30de-4032-8fc9-b0b8ebf8424c','{\"_fresh\":false,\"_permanent\":true,\"captcha_answer\":\"79365\"}','2021-03-15 12:19:16'),(15,'session:f3315e3f-e82c-4722-a10a-2df668851817','{\"_permanent\":true}','2021-03-15 12:27:19'),(16,'session:401aaec7-e4fa-4d54-91df-b202b28aaf4d','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:31'),(17,'session:c3377c82-ee0c-4e03-bd9a-f54fd437ece8','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:32'),(18,'session:19cc3c78-0ee2-473a-8fea-ee82207d85e1','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:32'),(19,'session:4423b65e-5632-4488-a60e-3ce7c4f0f10e','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:32'),(20,'session:bd328491-9cbd-4dee-b458-5c01fe3b9e05','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:33'),(21,'session:740ee07e-3faa-4af5-9251-5bd45e253744','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:33'),(22,'session:81195167-1803-4e37-8c14-48c30ebc7956','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:33'),(23,'session:4f94dcb5-6bba-4296-a95d-85cb7813dc97','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:33'),(24,'session:ffaca4b2-6ad2-42f5-b6d4-4ba0d3e92799','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:34'),(25,'session:538b5b33-a73a-4db5-8226-d480dff363d7','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:34'),(26,'session:995f7e1e-5ac2-4481-81b6-67fb71f3c0cf','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:34'),(27,'session:4a7f7080-1e48-4f09-9e13-b8fead08d7ac','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:34'),(28,'session:ca6049c1-519e-4821-8d12-c4bd55e10508','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:35'),(29,'session:82607a4c-d27a-4313-895e-8f95135b2ed0','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:35'),(30,'session:776d2db1-ffe5-4003-8aed-c09313d3820c','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:36'),(31,'session:72bf3f43-f81a-4617-917a-2de8fa6ba246','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:36'),(32,'session:62e6ca65-8659-4cd3-88f8-a2adbaf43f8b','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:36'),(33,'session:c8b451bd-f02a-40a6-b042-21207f0caf43','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:37'),(34,'session:b2bc5808-5b15-4f37-b501-b9bded241681','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:37'),(35,'session:56c268c7-2f91-4ef1-944e-8c608e6d5a53','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:37'),(36,'session:1239e458-0ca4-4c9b-aa0e-3afa751804b5','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:37'),(37,'session:aeae77f5-02ef-4669-8ce4-4e28e9dca27d','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:38'),(38,'session:a3d8a949-ee8a-4f53-b582-0cf100f3bc28','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:38'),(39,'session:d4ab8a80-feb4-4062-ad29-9a2e7992c371','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:38'),(40,'session:399c942d-d1b9-4c5d-b321-b52a70482995','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:39'),(41,'session:5464ca3a-bed0-4b61-a588-be2a7e03bf6a','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:39'),(42,'session:cce667ea-61e8-44aa-be5d-47737ead7316','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:39'),(43,'session:8f7e9796-4178-464b-a91b-08899a4cd4ae','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:39'),(44,'session:2c9714f4-1837-4e02-a88c-956569337d06','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:40'),(45,'session:a9ca3c4a-d637-4bb0-9939-c34783114947','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:41'),(46,'session:ce8747db-08ac-4f62-8148-ff522ff20ddc','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:42'),(47,'session:3cbd8877-814a-4765-8a64-f2be1940ba86','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:42'),(48,'session:fd2f2da0-2f07-485b-961d-9767665e4926','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:43'),(49,'session:8d6a5ca4-a677-41f2-b8fe-e4228935656f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:43'),(50,'session:6dd1bc64-d940-4ade-9d0c-10ec9d14c5e3','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:43'),(51,'session:3442f730-f2c2-45ab-aaed-22f9e052cd9f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:44'),(52,'session:1b80bf79-7aa6-4b29-a5a6-94d044a0fcc6','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:44'),(53,'session:1581367d-fd1a-4386-abf1-fd04f2b21435','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:44'),(54,'session:8db31a4f-e71b-4f2c-94ae-4dee5a3aeb43','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:45'),(55,'session:46fd279a-849d-4cee-b797-fe2b9a0fd958','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:45'),(56,'session:641e1a08-033f-4465-ad18-00bdd7ac8202','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:45'),(57,'session:6dea4e4c-5d10-4cf7-8d9f-af982deb80d4','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:45'),(58,'session:755ca5d7-d99f-42a0-ad77-665e5a164e48','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:46'),(59,'session:738c113e-b169-4aa7-ad6f-701847d14581','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:46'),(60,'session:084a9abc-f8a6-4618-a4ea-2320d42a3201','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:46'),(61,'session:12fc7254-4dc9-4e4c-94b7-b0908622a027','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:47'),(62,'session:4328f48d-09dd-4e74-a294-20c621102c3c','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:47'),(63,'session:773f3a4a-8a1f-4d3c-95f8-768a712797cb','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:47'),(64,'session:838cd897-ff33-4747-941a-202777879293','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:47'),(65,'session:e2d43272-ab01-4cba-89e4-088a513dec70','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:48'),(66,'session:e5aefa63-f94c-4ad6-9e71-713e3cd98608','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:48'),(67,'session:c153c310-9a06-4c09-87a0-fdd6726bc6ab','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:48'),(68,'session:c42f0c88-8e55-43fb-bc92-5425e934358f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:49'),(69,'session:5043ebb0-1f74-45f6-b9b0-dd1f76e18f2c','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:49'),(70,'session:fc554489-bb22-4423-89d1-ecff37b2c210','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:49'),(71,'session:9d83bd28-0880-4458-a1ab-f3ee2b48f947','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:49'),(72,'session:b64bfcd0-7102-430e-9dc3-b96e7bac89d8','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:50'),(73,'session:5e6dbae1-6886-4868-a5ef-12207fe61df7','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:50'),(74,'session:8dc9bd53-58a5-48de-9e5f-faa8a6e98844','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:50'),(75,'session:b3570931-95a4-4b92-a304-5b1249c6b66a','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:51'),(76,'session:8295b96b-5edf-4fd1-a7cd-3c7b8cb7b27e','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:51'),(77,'session:ab853bf4-5ef5-49bb-9192-a1554e3f2d0f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:51'),(78,'session:f45e058c-75b6-4afd-9766-d54870996b02','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:51'),(79,'session:e6454439-11b9-4572-a15c-5ee44523ae4d','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:52'),(80,'session:54ddab03-6af1-4cea-bce3-3db23e4e3c29','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:52'),(81,'session:7f36db1f-cdd7-4e8f-96f6-111920ded033','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:52'),(82,'session:9b2c6053-b5c5-4815-b49c-2fbe94349837','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:53'),(83,'session:1fbfd9dd-3583-4091-b7d4-6b0fa550752f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:53'),(84,'session:dbff6c8e-0324-41dc-95f5-f76215f16647','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:53'),(85,'session:bb0b4d6a-50ab-4480-a6e9-22c10b9a8ecf','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:53'),(86,'session:42ffc6b0-e5f5-4c6b-95c6-52aebf90519e','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:54'),(87,'session:671caf61-3c9c-437d-ab4e-7ecf37c5fc93','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:54'),(88,'session:d2de4dc8-02fb-4631-a039-5a791dacb1c1','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:54'),(89,'session:8cbdbeb5-c5df-4aa7-9282-4df5aab03219','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:54'),(90,'session:3e4052b9-1b68-414e-93b1-bed6345583a2','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:55'),(91,'session:760502c5-ec41-4a2e-adf1-9a80d75b1065','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:55'),(92,'session:43a4a8ce-cb3b-47e2-b4f2-26d7cd49031e','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:55'),(93,'session:160fe10b-b590-4ebf-8cc4-8c942207b10f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:56'),(94,'session:f618efe3-3d5a-43bb-9ee5-ab0b944af428','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:56'),(95,'session:440ac609-de70-4432-80bc-d67bfc15c604','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:56'),(96,'session:9456963d-bf68-474b-96b8-2717966965e2','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:56'),(97,'session:c75d3166-3b47-4bfb-af80-f93bfef96b1e','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:57'),(98,'session:b3e17093-1009-423a-aaa6-7cac31701a2c','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:57'),(99,'session:b64a8b1b-4d5d-4aff-a290-4975aafe8d2b','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:57'),(100,'session:fe2ab728-f77e-4552-9c93-6c77321a9f50','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:58'),(101,'session:768f3b13-f630-408c-8556-a5828f2c2db4','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:58'),(102,'session:73ffb523-3f43-4e6e-a64f-cbc22a7bcfd1','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:58'),(103,'session:19d36d36-1304-4183-9312-45b1e34a951f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:58'),(104,'session:59ec430f-fb17-4fd7-a728-896455354482','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:59'),(105,'session:8f4b4644-7eda-4403-8625-c27015fbc8fb','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:59'),(106,'session:a9bb8d95-d028-40f1-bb8f-5014200e6cb4','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:40:59'),(107,'session:ecf03360-dc20-4b45-b5e1-5f957aa53d77','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:00'),(108,'session:7b68ea71-1c0b-4948-b994-3737c63b7619','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:00'),(109,'session:e9f92c14-7dff-4710-b8fb-0d0f4c4b4a25','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:00'),(110,'session:807ab0cc-43b0-44bf-81c8-5e1053ef9782','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:00'),(111,'session:8ab5b567-233d-43fb-880d-3e9e78ea946f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:01'),(112,'session:ad6c371f-f4dd-43dd-95af-974e819cb18a','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:01'),(113,'session:af57840d-132f-437d-b7f0-39b21e686db0','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:01'),(114,'session:15d3675d-1e13-4e1a-8cc9-0d7474ba869c','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:02'),(115,'session:b7026967-d6f8-49a9-af06-088cf7f5579a','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:02'),(116,'session:9f60776b-fb5d-47bf-87b3-71abfec9b7cb','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:02'),(117,'session:11f8d163-1ea2-4030-bf08-6ebd90934cd2','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:02'),(118,'session:e013e666-9386-48e8-ba30-aec4b8d73641','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:03'),(119,'session:edd11f41-2377-485a-80a8-64bde6a95cd6','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:03'),(120,'session:83eaf9b5-342c-416c-aaf6-21b6047cba6a','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:03'),(121,'session:c92d853e-2089-4218-976e-c34f4b648046','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:04'),(122,'session:e35f0ada-8191-47b8-baf0-fb3341c6f0fa','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:04'),(123,'session:cc0917e3-5674-405c-9935-5c3335ab3633','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:04'),(124,'session:f98259bc-ddbb-4f52-89cf-846f183bd42d','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:05'),(125,'session:470894d6-34d4-47b6-b85b-a0572b8db700','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:05'),(126,'session:5eee055c-c81f-4f3b-831f-96391d56b46f','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:05'),(127,'session:ad1e8882-dfbd-4381-85ca-4764796873b5','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:05'),(128,'session:688d63c1-65b5-4698-91f8-38d11c246897','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:06'),(129,'session:06aa49de-e7cf-43d4-8ecc-c8b35acfd8b3','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:06'),(130,'session:31f877cc-44bf-4dbc-8ef5-0ca82aa6ac05','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:06'),(131,'session:39170e08-100c-4610-874f-e64903fa2ff3','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:06'),(132,'session:90beae8a-5fc1-4263-8b2d-53c6e5e2bc53','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:07'),(133,'session:d6497822-c3e3-4d3a-b100-367ca604601d','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:07'),(134,'session:25956bb4-7954-45ee-b0a7-e0148e18aeee','{\"_fresh\":false,\"_permanent\":true}','2021-03-15 12:41:07'),(135,'session:91cd8e92-75d8-4d94-b20d-3db6bcdcc259','{\"_permanent\":true}','2021-03-15 12:52:35'),(136,'session:267d7405-b84f-450c-8034-f6f2b9d25b75','{\"_fresh\":true,\"_id\":\"ba3ded0390e9f9de70d64858e1f9bec2fb680ef760b7fd3eb3ed33b73255ad410e3aa2d79bb40a7c0a989c333b3725e4be81d9946283e18dc66642db01e7ca0b\",\"_permanent\":true,\"_user_id\":1,\"captcha_answer\":null}','2021-03-15 13:04:16');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID_USR` int(11) NOT NULL AUTO_INCREMENT,
  `ID_RGN` int(11) DEFAULT NULL,
  `USERNAME_USR` varchar(45) DEFAULT NULL,
  `PASS_USR` varchar(100) DEFAULT NULL,
  `ETAT_USR` varchar(45) DEFAULT NULL,
  `TYPE_USR` varchar(45) DEFAULT NULL,
  `DATE_CREATE_USR` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_USR`),
  KEY `FK_REGION_CPT` (`ID_RGN`),
  CONSTRAINT `FK_REGION_CPT` FOREIGN KEY (`ID_RGN`) REFERENCES `region` (`ID_RGN`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1,'compt1','$2b$12$hFaoAque3qrC6bseVOdBSe7qBM7gSngZPaVyrAq5uIB1Da2xHE7BK','active','comptable','2021-02-27 15:13:35');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `validation`
--

DROP TABLE IF EXISTS `validation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `validation` (
  `ID_VALD` int(11) NOT NULL,
  `ID_ART` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `NOM_VALD` varchar(45) DEFAULT NULL,
  `DATE_VALD` datetime DEFAULT NULL,
  `SOLDE_VALD` double(50,2) DEFAULT NULL,
  PRIMARY KEY (`ID_VALD`),
  KEY `FK_PREV_ART` (`ID_ART`),
  KEY `FK_REFERENCE_15` (`ID_USR`),
  CONSTRAINT `FK_PREV_ART` FOREIGN KEY (`ID_ART`) REFERENCES `article` (`ID_ART`),
  CONSTRAINT `FK_REFERENCE_15` FOREIGN KEY (`ID_USR`) REFERENCES `user` (`ID_USR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `validation`
--

LOCK TABLES `validation` WRITE;
/*!40000 ALTER TABLE `validation` DISABLE KEYS */;
/*!40000 ALTER TABLE `validation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-15 14:23:08
