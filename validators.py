
from marshmallow import Schema, fields, validate

class CompteSchema(Schema):
    reg = r"^([a-zA-Z_ 0-9]+)?$"
    regmdp = r"^([a-zA-Z_$@0-9]+)?$"
    # id = fields.Int(dump_only=True)
    # username = fields.Nested(AuthorSchema, validate=must_not_be_blank)
    username = fields.Str(required=True, validate=validate.Regexp(reg))
    password = fields.Str(required=True, validate=validate.Regexp(regmdp))
    # posted_at = fields.DateTime(dump_only=True)

    # Allow client to pass author's full name in request body
    # e.g. {"author': 'Tim Peters"} rather than {"first": "Tim", "last": "Peters"}
    # @pre_load
    # def process_author(self, data, **kwargs):
        # author_name = data.get("author")
        # if author_name:
        #     first, last = author_name.split(" ")
        #     author_dict = dict(first=first, last=last)
        # else:
        #     author_dict = {}
        # data["author"] = author_dict
        # return data

