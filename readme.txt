

Les étapes a suivre pour déployer l'application:

    1 - installer anaconda (gestionnaire des environnements des solution de Python)

    2 - creer un nouveau environnement (exp: conda create --name env1 python=3.5)

    3 - activate env1 (pour entrer dans le nouveau environnement)

    4 - pip install -r requirements.txt  (pour importer les bibliothèques requis pour executer l'application)

    5 - cd /d chemin de l'application (exp: cd /d D:/nomenclature/)

    6 - python manage.py (pour executer l'application web)

    7 - Ouvrir le navigateur Web et saisir l'addresse http://localhost:80

    Note: username : admin
          password : hello2020$

          username : user1
          password : user2020
    Note 2: Il faut etre connecté à l'internet pour pouvoir télécharger les bibliothèques de python.

